template = app
target = testwsc

SOURCES += test_ws_client.cpp libwebsockets_extra.cpp

# libwss
DEFINES += DEBUG
QMAKE_CFLAGS += -include ./libwss/wss_config.h -g
QMAKE_CXXFLAGS += -include ./libwss/wss_config.h -g

INCLUDEPATH += ./libwss
win32 {
INCLUDEPATH += ./libwss/win32helpers Z:/librarys/vc-zlib/include Z:/cross/boost_1_46_1
} else {

}

win32 {
    SOURCES +=  ./libwss/win32helpers/gettimeofday.c
}
#DEFINES += LWS_EXT_GOOGLE_MUX
DEFINES += KP_LWS_CLIENT
# \"\\\"$$VERSION\\\"\" 
DEFINES += LWS_OPENSSL_SUPPORT LWS_OPENSSL_CLIENT_CERTS=\"\\\"/etc/pki/tls/certs/\\\"\" 
LIBS += -lssl               

SOURCES += ./libwss/base64-decode.c  \
            ./libwss/extension.c     \
            ./libwss/handshake.c     \
#            ./libwss/md5.c     \
#            ./libwss/sha-1.c         \
            ./libwss/client-handshake.c  \
            ./libwss/extension-deflate-stream.c  \
            ./libwss/extension-x-google-mux.c \
            ./libwss/libwebsockets.c  \
            ./libwss/parsers.c 
