// skycitengine.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-09-15 09:32:20 +0000
// Version: $Id: skycitengine.cpp 1050 2011-10-17 10:01:03Z drswinghead $
// 

#include "simplelog.h"

#include "skycit.h"
#include "websocketclient3.h"

#include "websocketserver3.h"
#include "skycitengine.h"

SkycitEngine *SkycitEngine::m_inst = NULL;
SkycitEngine::SkycitEngine(QObject *parent)
    : QThread(parent)
{
    this->sct = NULL;
    this->sigbr = NULL;
    this->mws = NULL;

    this->app_name_prefix = "app_skynet_";
    this->app_name_main = QString("%1main").arg(this->app_name_prefix);
    // this->app_name_self = QString("%1self").arg(this->app_name_prefix);
    // set at proper time
}

SkycitEngine *SkycitEngine::instance()
{
    if (SkycitEngine::m_inst == NULL) {
        SkycitEngine::m_inst = new SkycitEngine();
    }
    return SkycitEngine::m_inst;
}

SkycitEngine::~SkycitEngine()
{
    qLogx()<<"";
}

bool SkycitEngine::initialize()
{
    // this object must be in main gui thread
    this->sct = new Skycit("karia2");
    this->sct->setRunAsClient();
    QObject::connect(this->sct, SIGNAL(skypeError(int, QString, QString)),
                     this, SLOT(onSkypeError(int, QString, QString)));
    QObject::connect(this->sct, SIGNAL(skypeNotFound()),
                     this, SLOT(onSkypeNotFound()));
    
    QObject::connect(this->sct, SIGNAL(connected(QString)), this, SLOT(onSkypeConnected(QString)));
    QObject::connect(this->sct, SIGNAL(realConnected(QString)),
                     this, SLOT(onSkypeRealConnected(QString)));

    QObject::connect(this->sct, SIGNAL(newApp2AppCreated(QString)),
                     this, SLOT(onSkypeApp2AppCreated(QString)));
    QObject::connect(this->sct, SIGNAL(newStreamCreated(QString,QString,int)),
                     this, SLOT(onNewStreamCreated(QString,QString,int)));
    QObject::connect(this->sct, SIGNAL(streamClosed(QString)), this, SLOT(onStreamClosed(QString)));
    QObject::connect(this->sct, SIGNAL(streamConnectingDone(QString)), this, SLOT(onStreamConnectingDone(QString)));

    QObject::connect(this->sct, SIGNAL(dataInStream(QString,QString)), this, SLOT(onStreamData(QString,QString)));

    QObject::connect(this->sct, SIGNAL(UserStatus(QString,int)),
                     this, SLOT(onSkypeUserStatus(QString,int)));

    QObject::connect(this->sct, SIGNAL(newCallArrived(QString,QString,int)),
                     this, SLOT(onSkypeCallArrived(QString,QString,int)));
    QObject::connect(this->sct, SIGNAL(callHangup(QString,QString,int)),
                     this, SLOT(onSkypeCallHangup(QString,QString,int)));

    this->old_thread = this->thread();
    // confirm all this's slot run in this run thread
    this->moveToThread(this);
    QObject::connect(this, SIGNAL(started()), this, SLOT(onSCEThreadStarted()));

    this->start();

    return true;
}

bool SkycitEngine::destroy()
{
    this->ws_sigc_error.disconnect();
    this->ws_sigc_started.disconnect();

    return true;
}

bool SkycitEngine::vestart()
{
    //Q_ASSERT(!this->ws_sigc_new_conn.connected());
    Q_ASSERT(!this->ws_sigc_msg.connected());
    //Q_ASSERT(!this->ws_sigc_close.connected());

    // this->ws_sigc_new_conn = this->mws->new_connection.connect(boost::bind(&SCBSignalBridge::on_new_websocket_connection, this->sigbr));
    this->ws_sigc_msg = this->mws->new_message.connect(boost::bind(&SCBSignalBridge::on_websocket_message, this->sigbr, _1, _2));
    // this->ws_sigc_close = this->mws->connection_closed.connect(boost::bind(&SCBSignalBridge::on_websocket_connection_closed, this->sigbr, _1));

    return true;
}

bool SkycitEngine::vestop()
{
    //Q_ASSERT(this->ws_sigc_new_conn.connected());
    Q_ASSERT(this->ws_sigc_msg.connected());
    //Q_ASSERT(this->ws_sigc_close.connected());

    //this->ws_sigc_new_conn.disconnect();
    this->ws_sigc_msg.disconnect();
    //this->ws_sigc_close.disconnect();

    // delete this->sigbr; this->sigbr = NULL;
    // this->quit();

    return true;
}

//bool SkycitEngine::isRunning()
//{
//    //
//    // return this->isRunning();
//}

bool SkycitEngine::isStarted()
{
    qLogx()<<"";
    qLogx()<<this->isRunning();
    qLogx()<<this->isFinished();
    qLogx()<<this->mws;
    if (this->mws) {
        qLogx()<<this->mws->isRunning();
    }
    qLogx()<<this->ws_sigc_msg.connected();
    //qLogx()<<this->ws_sigc_new_conn.connected();
    //qLogx()<<this->ws_sigc_close.connected();

    return this->isRunning() &&  // why call this method block and cpu 100%
        // !this->isFinished() &&
        this->mws != NULL && this->mws->isRunning() 
        && this->ws_sigc_msg.connected()
        ;//&& this->ws_sigc_new_conn.connected() && this->ws_sigc_close.connected();
}


void SkycitEngine::run()
{
    qLogx()<<""<<QThread::currentThread();

    bool bret = false;
   
    this->sigbr = new SCBSignalBridge();

    QObject::connect(this->sigbr, SIGNAL(websocket_error(int)), this, SLOT(onWS_websocket_error(int)));
    QObject::connect(this->sigbr, SIGNAL(websocket_started()), this, SLOT(onWS_websocket_started()));
    //QObject::connect(this->sigbr, SIGNAL(new_websocket_connection()), this, SLOT(onWS_new_websocket_connection()));
    QObject::connect(this->sigbr, SIGNAL(new_websocket_connection_with(int)), this, SLOT(onWS_new_websocket_connection_with(int)));
    QObject::connect(this->sigbr, SIGNAL(websocket_message(const QString&, int)),
                     this, SLOT(onWS_websocket_message(const QString &, int)));
    QObject::connect(this->sigbr, SIGNAL(websocket_connection_closed(int)),
                     this, SLOT(onWS_websocket_connection_closed(int)));

    this->mws = WebSocketServer3::instance();
    this->ws_sigc_error = this->mws->error.connect(boost::bind(&SCBSignalBridge::on_websocket_error, this->sigbr, _1));
    this->ws_sigc_started = this->mws->started.connect(boost::bind(&SCBSignalBridge::on_websocket_started, this->sigbr));
    this->ws_sigc_new_conn = this->mws->new_connection_with.connect(boost::bind(&SCBSignalBridge::on_new_websocket_connection_with, this->sigbr, _1));
    this->ws_sigc_close = this->mws->connection_closed.connect(boost::bind(&SCBSignalBridge::on_websocket_connection_closed, this->sigbr, _1));
    // this->mws->error.connect(boost::bind(&SkycitEngine::onWS_websocket_error, this, _1));
    // this->mws->started.connect(boost::bind(&SkycitEngine::onWS_websocket_started, this));
    // this->mws->new_connection.connect(boost::bind(&SkycitEngine::onWS_new_websocket_connection, this));
    // this->mws->new_message.connect(boost::bind(&SkycitEngine::onWS_websocket_message, this, _1, _2));
    // this->mws->connection_closed.connect(boost::bind(&SkycitEngine::onWS_websocket_connection_closed, this, _1));

    if (!this->mws->isRunning()) {
        // TODO start here maybe has block 
        bret = this->mws->start();
    }

    // this->runin();
    this->exec();
}

// void SkycitEngine::runin()
// {
//     qLogx()<<"";


//     // this->moveToThread(this->sigbr);
// }

void SkycitEngine::onSCEThreadStarted()
{
    qLogx()<<""<<this->thread();
    // confirm all this's slot run in this run thread
    // this->moveToThread(this);
    // emit runin_it();
    if (this->mws != NULL && this->mws->isStarted()) {
        emit this->skycit_engine_started();
    }
}

void SkycitEngine::onWS_websocket_error(int eno)
{
    qLogx()<<eno;
}

void SkycitEngine::onWS_websocket_started()
{
    qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);
    // this->sip_engine_error(eno);
    if (this->isRunning()) {
        // 怎么防止这个信号会被发送两次呢。
        emit this->skycit_engine_started();
    }
}

// TODO support multi client connect, from desktop app or web app
//void SkycitEngine::onWS_new_websocket_connection()
//{
//    qLogx()<<"";
//    qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);
//    std::string mvstr;
//    int cseq = this->mws->nextPendingConnection();
//    assert(cseq > 0);

//    if (this->su1.cseq != -1) {
//        qLogx()<<"only allow 1 client:"<<this->su1.cseq<<", drop this."<<cseq;
//    }

//    // mvstr = this->mws->conn_payload_path(cseq);
//    // this->wsconns.left.insert(boost::bimap<int,int>::left_value_type(cseq, cseq));

//    // TODO check connect validation
//    this->su1.cseq = cseq;

//    // ////////
//    // this->move_runin(boost::bind(&SkycitEngine::on_send_codec_list, this));
//}

void SkycitEngine::onWS_new_websocket_connection_with(int cseq)
{
    qLogx()<<"";
    qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);
    std::string mvstr;
    // int cseq = this->mws->nextPendingConnection();
    assert(cseq > 0);

    if (this->su1.cseq != -1) {
        qLogx()<<"only allow 1 client:"<<this->su1.cseq<<", drop this."<<cseq;
    }

    // mvstr = this->mws->conn_payload_path(cseq);
    // this->wsconns.left.insert(boost::bimap<int,int>::left_value_type(cseq, cseq));

    // TODO check connect validation
    this->su1.cseq = cseq;

    // ////////
    // this->move_runin(boost::bind(&SkycitEngine::on_send_codec_list, this));
}

void SkycitEngine::onWS_websocket_message(const QString &msg, int cseq)
{
    qLogx()<<""<<msg<<cseq;
    qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);

    this->process_backend_ws_ctrl_message(msg.toStdString(), cseq);

    // char tbuf[512] = {0};
    // snprintf(tbuf, sizeof(tbuf), "Returned by server via %d ,%s", cseq, msg.toAscii().data());  
    // std::string ret_msg = std::string(tbuf, strlen(tbuf));

    // this->move_runin(boost::bind(&SkycitEngine::process_ctrl_message, this, msg, cseq));

    // for test only
    // this->mws->wssend(cseq, ret_msg);
}

void SkycitEngine::onWS_websocket_connection_closed(int cseq)
{
    // assert(this->su1.cseq == cseq);
    // this->su1.cseq = -1;
    qLogx()<<""<<cseq;
}

bool SkycitEngine::process_backend_ws_ctrl_message(const std::string &msg, int cseq)
{
    qLogx()<<"processing: "<<cseq<<msg.length()<<msg.c_str();

    bool bret = false;
    int iret = 0;
    int cmd_id = -1;
    int cmd_len;
    int call_id = -1;
    QString str;
    QString wsuri = "wss://202.108.12.212:8000/%1/desktop/";
    this->skygw_name = "liuguangzhao01";
    QHash<QString, A2AStreamUnit> ahash;

    CmdMakeCall mc;
    CmdHangupCall cmd_hc;
    CmdRegister reger;
    CmdCallDTMF cmd_dtmf;

    cmd_id = InterMessage().jpack_cmdid(msg);

    switch (cmd_id) {
    case IPC_NO_MAKE_CALL:
        InterMessage().junpack_message(mc, msg);
        this->su1.mc_str = msg;
        this->su1.cmd_mc = mc;

        // 为什么不在register指令的时候初始化这个连接
        // 这两个指令哪个会更先到达呢
        // this->wsc2 = new WebSocketClient3("WSS://127.0.0.1:18080/hahaha/");
//        this->wsc2 = new WebSocketClient3(wsuri.arg(QString::fromStdString(mc.caller_name)));
//        QObject::connect(this->wsc2, SIGNAL(onConnected(QString)), this, SLOT(onon_ws_client_connected(QString)));
//        QObject::connect(this->wsc2, SIGNAL(onError()), this, SLOT(onon_ws_client_error()));
//        QObject::connect(this->wsc2, SIGNAL(onDisconnected()), this, SLOT(onon_ws_client_disconnected()));
//        QObject::connect(this->wsc2, SIGNAL(onWSMessage(QByteArray)), this, SLOT(onon_ws_client_message(QByteArray)));

//        this->wsc2->connectToServer();

        ///////////////
        ahash[this->skygw_name] = A2AStreamUnit();
        this->app2appStates[this->app_name_main] = ahash;
        this->sct->newStream(this->app_name_main, this->skygw_name);

        // InterMessage().junpack_message(mc, msg);
        // this->su1.mc_str = msg;
        // this->su1.cmd_mc = mc;
        //     bret = this->make_call(mc.acc_id, mc.caller_name, mc.callee_phone, mc.sip_server,
        //                            call_id, mc);
        //     if (call_id != PJSUA_INVALID_ID) {
        //         this->su1.call_id = call_id;
        //     }
        break;
     case IPC_NO_REGISTER:
         // this->sct->connectToSkype();
         //     InterMessage().junpack_message(reger, msg);
         //     this->su1.reg_str = msg;
         //     this->su1.cmd_reg = reger;
         //     bret = this->register_account(reger.display_name, reger.user_name, reger.sip_server,
         //                                   !reger.unregister, reger.password, msg);
         break;
    case IPC_NO_HANGUP_CALL:
        InterMessage().junpack_message(cmd_hc, msg);
        call_id = cmd_hc.call_id;
        // ref siproom.h
        str = QString("107$%1$fffffff$0").arg(QString::fromStdString(cmd_hc.caller_name));

        this->sct->writeToStream(this->app_name_self, this->skygw_name, str.toAscii());

//        if (this->wsc2 == NULL || this->wsc2->isConnected() == false) {
//            qLogx()<<"ws connect not exist, internal ctrl error.";
//            Q_ASSERT(this->wsc2 != NULL && this->wsc2->isConnected());
//        }
//        iret = this->wsc2->sendMessage(str.toAscii());
//        Q_ASSERT(iret == 0);
        break;
    case IPC_NO_CALL_DTMF:
        InterMessage().junpack_message(cmd_dtmf, msg);
        call_id = cmd_dtmf.call_id;
        // ref siproom.h
        str = QString("105$%1$fffffff$%2").arg(QString::fromStdString(cmd_dtmf.caller_name))
                .arg(QString::fromStdString(cmd_dtmf.dtmf_chars));

        this->sct->writeToStream(this->app_name_self, this->skygw_name, str.toAscii());

//        if (this->wsc2 == NULL || this->wsc2->isConnected() == false) {
//            qLogx()<<"ws connect not exist, internal ctrl error.";
//            Q_ASSERT(this->wsc2 != NULL && this->wsc2->isConnected());
//        }
//        iret = this->wsc2->sendMessage(str.toAscii());
//        Q_ASSERT(iret == 0);
        break;
    default:
        qLogx()<<"Unsupported cmd:"<<cmd_id;
        break;
    }

    return true;
}

////////////////////

void SkycitEngine::onon_ws_client_connected(QString rpath)
{
    qLogx()<<rpath;
    QString num = QString::fromStdString(this->su1.cmd_mc.callee_phone);
    QString cmd = QString("101$%1$%2").arg(this->sct->handlerName()).arg(num);
    this->wsc2->sendMessage(cmd.toAscii());

    // QPushButton *btn = this->uiw->pushButton_5;
    // btn->setEnabled(true);

    // int cnter = 0;
    // std::string jcstr;
    // while (this->cmds_queue_before_connected.count() > 0) {
    //     qLogx()<<++cnter;
    //     jcstr = this->cmds_queue_before_connected.at(0);
    //     this->cmds_queue_before_connected.remove(0);

        
    //     this->wsc2->sendMessage(jcstr);
    // }
}

void SkycitEngine::onon_ws_client_disconnected()
{
    qLogx()<<"";
    // 如果区别被服务器异常关闭，还是客户端主动关闭呢。
}

void SkycitEngine::onon_ws_client_error()
{
    qLogx()<<"";

    // qLogx()<<error<<errmsg;
    // if (error == WebSocketClient::EWS_HANDSHAKE) {
    //     log_output(LT_USER, tr("网络协议错误，2秒后重试连接服务器。。。"));
    //     // 这种直接调用的情况下，被调用方法接收到的 sender()仍旧是调用处的sender()!!!
    //     // this->onCallPstn();
    //     QTimer::singleShot(100, this, SLOT(onCallPstn()));
    // } else if (error == QAbstractSocket::RemoteHostClosedError) {
    //     // 通话完成？还是非正常连接中断？
    // } else {
    //     log_output(LT_USER, tr("网络错误，3秒后重试连接服务器。。。") + errmsg);
    //     // this->onCallPstn();
    //     QTimer::singleShot(200, this, SLOT(onCallPstn()));
    // }
}

void SkycitEngine::onon_ws_client_message(QByteArray msg)
{
    qLogx()<<""<<msg;
    bool bret;
    std::string semsg;

    // semsg = std::string(msg.data(), msg.length());
    // bret = this->process_ctrl_message(semsg);

    CmdSCEState cmd_sce_state;
    QString forwarder_name;
    QString router_name;
    QStringList fields = QString(msg).split("$");
    QString log_msg;

    cmd_sce_state.raw_msg = std::string(msg.data(), msg.length());
    cmd_sce_state.state = fields.at(0).toInt();
    switch (cmd_sce_state.state) {
    case 102:
        Q_ASSERT(fields.at(1) == this->mSkype->handlerName());
        if (fields.at(2) == "200 OK") {
            Q_ASSERT(fields.at(3).trimmed().toInt() >= 1);
            router_name = fields.at(4).trimmed();

            this->sct->callFriend(router_name);
        }
        cmd_sce_state.caller_name = fields.at(1).toStdString();
        cmd_sce_state.router_name = router_name.toStdString();
        cmd_sce_state.state_text = fields.at(2).toStdString();
        //     tmps = fields.at(2).split("\n");
        //     if (tmps.at(0).trimmed() == "200 OK") {
        //         Q_ASSERT(tmps.at(1).trimmed().toInt() >= 1);
        //         router_name = tmps.at(2).trimmed();
            
        //         this->mSkype->callFriend(router_name);
        //     }
        break;
    case 104:
        this->wsc2->disconnect();
        delete this->wsc2;
        this->wsc2 = NULL;
        //     log_msg = "线路忙，请稍后再拨。";
        //     this->log_output(LT_USER, log_msg);
        
        //     // 是不是需要这些状态控制呢？ // 不需要，服务器端还会发送108通话结束指令。
        //     // this->onDynamicSetVisible(this->m_call_state_widget, false);
        //     // // 关闭网络连续，如果有的话。
        //     // this->wscli.reset();
        //     // this->uiw->pushButton_4->setEnabled(true);
        break;
    case 106:
        cmd_sce_state.state_text = (QString("FCall notice: ") + fields.at(4)).toStdString();
        //     log_msg = QString("FCall notice: ") + fields.at(4);
        //     this->log_output(LT_DEBUG, log_msg);
        break;
    case 108:
        this->wsc2->disconnect();
        delete this->wsc2; this->wsc2 = NULL;
        //     log_msg = "通话结束。";
        //     // log_msg = QString("<p><img src='%1'/> %2</p>").arg(":/skins/default/info.png").arg(log_msg);
        //     // this->uiw->plainTextEdit->appendPlainText(log_msg);
        //     // this->uiw->plainTextEdit->appendHtml(log_msg);
        //     this->log_output(LT_USER, log_msg);

        //     this->onDynamicSetVisible(this->m_call_state_widget, false);

        //     // 关闭网络连续，如果有的话。
        //     this->wscli.reset();
        //     this->uiw->pushButton_4->setEnabled(true);
        break;
    case 110:
        //     log_msg = "不支持通话挂起功能，请尽快恢复，否则对方可能因听不到您的声音而挂断。";
        //     this->log_output(LT_USER, log_msg);
        break;
     case 112:
         //     log_msg = "对方已经接通，计时开始。";
         //     this->log_output(LT_USER, log_msg);
         break;
    case 114:
        //     log_msg = "可能会有2-3秒静音时间，请稍后。。。"; // may be mute 3-5s
        //     this->log_output(LT_USER, log_msg);
        break;
    case 116:
        //     log_msg = "分配通话线路。。。";// Allocate circuitry...
        //     this->log_output(LT_USER, log_msg);
        break;
    case 117:
        //     log_msg = "连接对方话机。。。"; // Info: connect pstn network.
        //     this->log_output(LT_USER, log_msg);
        break;
    case 118:
        //     log_msg = QString("对方已挂机，代码:%1").arg(fields.at(5));
        //     // log_msg = QString("<p><img src='%1'/> %2</p>").arg(":/skins/default/info.png").arg(log_msg);
        //     // this->uiw->plainTextEdit->appendPlainText(log_msg);
        //     // this->uiw->plainTextEdit->appendHtml(log_msg);
        //     this->log_output(LT_USER, log_msg);

        //     switch (fields.at(5).toInt()) {
        //     case 603:
        //         break;
        //     default:
        //         break;
        //     };
        break;
    default:
        qLogx()<<"Unknwon ws msg:"<<msg;
        break;
    }

    std::string cmdstr = InterMessage().jpack_message(cmd_sce_state);

    // qLogx()<<"mws msg to top ui:"<<this->su1.cseq<<cmdstr.length()<<QString::fromStdString(cmdstr);
    // send to which wsi???
    int iret = this->mws->wssend(this->su1.cseq, cmdstr);
    // Q_ASSERT(iret == 0);
}

// app2app client message from remote gw server
bool SkycitEngine::onApp2AppMessage(const QString &appName, const QString &contact, const QString &pmsg)
{
    QByteArray msg = pmsg.toAscii();
    qLogx()<<""<<msg;
    bool bret;
    std::string semsg;

    // semsg = std::string(msg.data(), msg.length());
    // bret = this->process_ctrl_message(semsg);

    CmdSCEState cmd_sce_state;
    QString forwarder_name;
    QString router_name;
    QStringList fields = QString(msg).split("$");
    QString log_msg;

    cmd_sce_state.raw_msg = std::string(msg.data(), msg.length());
    cmd_sce_state.state = fields.at(0).toInt();
    switch (cmd_sce_state.state) {
    case 102:
        Q_ASSERT(fields.at(1) == this->mSkype->handlerName());
        if (fields.at(2) == "200 OK") {
            Q_ASSERT(fields.at(3).trimmed().toInt() >= 1);
            router_name = fields.at(4).trimmed();

            this->sct->callFriend(router_name);
        }
        cmd_sce_state.caller_name = fields.at(1).toStdString();
        cmd_sce_state.router_name = router_name.toStdString();
        cmd_sce_state.state_text = fields.at(2).toStdString();
        //     tmps = fields.at(2).split("\n");
        //     if (tmps.at(0).trimmed() == "200 OK") {
        //         Q_ASSERT(tmps.at(1).trimmed().toInt() >= 1);
        //         router_name = tmps.at(2).trimmed();

        //         this->mSkype->callFriend(router_name);
        //     }
        break;
    case 104:
//        this->wsc2->disconnect();
//        delete this->wsc2;
//        this->wsc2 = NULL;
        //     log_msg = "线路忙，请稍后再拨。";
        //     this->log_output(LT_USER, log_msg);

        //     // 是不是需要这些状态控制呢？ // 不需要，服务器端还会发送108通话结束指令。
        //     // this->onDynamicSetVisible(this->m_call_state_widget, false);
        //     // // 关闭网络连续，如果有的话。
        //     // this->wscli.reset();
        //     // this->uiw->pushButton_4->setEnabled(true);
        break;
    case 106:
        cmd_sce_state.state_text = (QString("FCall notice: ") + fields.at(4)).toStdString();
        //     log_msg = QString("FCall notice: ") + fields.at(4);
        //     this->log_output(LT_DEBUG, log_msg);
        break;
    case 108:
//        this->wsc2->disconnect();
//        delete this->wsc2; this->wsc2 = NULL;
        Q_ASSERT(this->app2appStates.contains(appName));
        Q_ASSERT(this->app2appStates.value(appName).contains(contact));
        this->app2appStates[appName][contact].use_finished = true;
        if (this->app2appStates[appName][contact].use_finished == true
                && this->app2appStates[appName][contact].connecting_done_evt == true) {
            // 可以关闭这个连接了
            this->sct->deleteStream(appName, contact);
            this->app2appStates[appName].remove(contact);
        }
        //今天发现一个bug，当收到这个通话结束消息时，skype呼叫还在连接中。

        //     log_msg = "通话结束。";
        //     // log_msg = QString("<p><img src='%1'/> %2</p>").arg(":/skins/default/info.png").arg(log_msg);
        //     // this->uiw->plainTextEdit->appendPlainText(log_msg);
        //     // this->uiw->plainTextEdit->appendHtml(log_msg);
        //     this->log_output(LT_USER, log_msg);

        //     this->onDynamicSetVisible(this->m_call_state_widget, false);

        //     // 关闭网络连续，如果有的话。
        //     this->wscli.reset();
        //     this->uiw->pushButton_4->setEnabled(true);
        break;
    case 110:
        //     log_msg = "不支持通话挂起功能，请尽快恢复，否则对方可能因听不到您的声音而挂断。";
        //     this->log_output(LT_USER, log_msg);
        break;
     case 112:
        cmd_sce_state.caller_name = fields.at(1).toStdString();
        // cmd_sce_state.router_name = router_name.toStdString();
        cmd_sce_state.forwarder_name = fields.at(2).toStdString();
        cmd_sce_state.backend_call_id = fields.at(3).toInt();
        cmd_sce_state.state_text = fields.at(4).toStdString();
         //     log_msg = "对方已经接通，计时开始。";
         //     this->log_output(LT_USER, log_msg);
         break;
    case 114:
        cmd_sce_state.caller_name = fields.at(1).toStdString();
        // cmd_sce_state.router_name = router_name.toStdString();
        cmd_sce_state.forwarder_name = fields.at(2).toStdString();
        cmd_sce_state.backend_call_id = fields.at(3).toInt();
        cmd_sce_state.state_text = fields.at(4).toStdString();
        //     log_msg = "可能会有2-3秒静音时间，请稍后。。。"; // may be mute 3-5s
        //     this->log_output(LT_USER, log_msg);
        break;
    case 116:
        //     log_msg = "分配通话线路。。。";// Allocate circuitry...
        //     this->log_output(LT_USER, log_msg);
        break;
    case 117:
        //     log_msg = "连接对方话机。。。"; // Info: connect pstn network.
        //     this->log_output(LT_USER, log_msg);
        break;
    case 118:
        //     log_msg = QString("对方已挂机，代码:%1").arg(fields.at(5));
        //     // log_msg = QString("<p><img src='%1'/> %2</p>").arg(":/skins/default/info.png").arg(log_msg);
        //     // this->uiw->plainTextEdit->appendPlainText(log_msg);
        //     // this->uiw->plainTextEdit->appendHtml(log_msg);
        //     this->log_output(LT_USER, log_msg);

        //     switch (fields.at(5).toInt()) {
        //     case 603:
        //         break;
        //     default:
        //         break;
        //     };
        break;
    default:
        qLogx()<<"Unknwon ws msg:"<<msg;
        break;
    }

    std::string cmdstr = InterMessage().jpack_message(cmd_sce_state);

    // qLogx()<<"mws msg to top ui:"<<this->su1.cseq<<cmdstr.length()<<QString::fromStdString(cmdstr);
    // send to which wsi???
    int iret = this->mws->wssend(this->su1.cseq, cmdstr);
    // Q_ASSERT(iret == 0);
    return true;
}

/////////////////

void SkycitEngine::onConnectSkype()
{
    // this->log_output(LT_USER, "正在连接Skype API。。。，");
    // this->log_output(LT_USER, "请注意Skype客户端弹出的认证提示。");
    // this->uiw->pushButton_3->setEnabled(false);
    // this->onInitPstnClient();

    qLogx()<<"Skype name:"<<this->sct->handlerName();
}

void SkycitEngine::onConnectApp2App()
{
    // QString skypeName = this->uiw->lineEdit->text();
    // this->mtun->setStreamPeer(skypeName);

    // QStringList contacts = this->mSkype->getContacts();
    // qDebug()<<skypeName<<contacts;

    // this->mSkype->newStream(skypeName);
    // this->mSkype->newStream("drswinghead");
}

void SkycitEngine::onSkypeNotFound()
{
    qLogx()<<"";
    // this->m_call_button_disable_count.ref();
    // this->uiw->pushButton_3->setEnabled(true);
    // this->log_output(LT_USER, QString("未安装或未登陆Skype客户端"));
}

void SkycitEngine::onSkypeConnected(QString user_name)
{
    qLogx()<<"Waiting handler name:"<<user_name;
}

void SkycitEngine::onSkypeRealConnected(QString user_name)
{
    qLogx()<<"Got handler name:"<<user_name;
    // this->uiw->label_3->setText(user_name);

    // count == 0
    // if (this->m_call_button_disable_count.deref() == false) {
    //     this->uiw->pushButton_4->setEnabled(true);
    // }

    this->app_name_self = QString("%1%2").arg(this->app_name_prefix).arg(user_name);

    // this->log_output(LT_USER, "连接Skype API成功，用户名：" + user_name);
    CmdResolvName cmd;
    cmd.name = user_name.toStdString();

    std::string jstr = InterMessage().jpack_message(cmd);
    
    this->mws->wssend(this->su1.cseq, jstr);

    // QString app_name = "app_skytun_main";
    // this->sct->newApp(app_name);
}

void SkycitEngine::onSkypeUserStatus(QString str_status, int int_status)
{
    qLogx()<<__FILE__<<__LINE__<<__FUNCTION__<<str_status<<int_status;
    // QPixmap icon = QPixmap(":/skins/default/status_offline.png");
    // switch (int_status) {
    // case SS_ONLINE:
    // case SS_INVISIBLE:
    // case SS_SKYPEME:
    //     icon = QPixmap(":/skins/default/status_online.png");
    //     break;
    // case SS_AWAY:
    //     icon = QPixmap(":/skins/default/status_away.png");
    //     break;
    // case SS_DND:
    //     icon = QPixmap(":/skins/default/status_dnd.png");
    //     break;
    // case SS_NA:
    //     icon = QPixmap(":/skins/default/status_busy.png");
    //     break;
    // case SS_OFFLINE:
    //     break;
    // default:
    //     break;
    // }

    // this->uiw->label_14->setPixmap(icon);
    
    // QString old_tooltip = this->uiw->label_14->toolTip();
    // QString new_tooltip = old_tooltip.split(":").at(0) + ": " + str_status;
    // this->uiw->label_14->setToolTip(new_tooltip);
}

void SkycitEngine::onSkypeError(int code, QString emsg, QString rcmd)
{
    qLogx()<<code<<emsg<<rcmd;
    // cmd "#720 CREATE APPLICATION app_skytun_xxxxxx"
    QString cmd = rcmd.right(rcmd.length() - rcmd.indexOf(' ') - 1);
    QString app_name;

    switch (code) {
    case 541:
        app_name = rcmd.right(rcmd.length() - rcmd.lastIndexOf(' ') - 1);
        qLogx()<<"Resend create app command:"<<cmd<<app_name;
        // 如果是其他进程创建的，则这无法删除。
//        this->sct->deleteApp(app_name);
//        this->sct->newApp(app_name);
        break;
    default:
        break;
    }
}

void SkycitEngine::onSkypeApp2AppCreated(const QString &appName)
{
    qLogx()<<appName;

    if (appName == this->app_name_main) {

    } else {

    }
}

void SkycitEngine::onNewStreamCreated(const QString &appName, const QString &contact, int sid)
{
    qLogx()<<appName<<contact<<sid;
    if (appName == this->app_name_main) {
        // 也许要在这处理一些控制标记之类的变量
    } else {
        // 两端可以传输数据了。
        // QString data = "aaaaaaaaaaaaaaafsdfnsofewfaewf中只为经发灵楥国...";
        // this->sct->writeToStream(appName, contact, data.toAscii());
        QString num = QString::fromStdString(this->su1.cmd_mc.callee_phone);
        QString cmd = QString("101$%1$%2").arg(this->sct->handlerName()).arg(num);
        // this->wsc2->sendMessage(cmd.toAscii());
        this->sct->writeToStream(appName, contact, cmd.toAscii());
    }
}

void SkycitEngine::onStreamClosed(const QString &appName)
{
    qLogx()<<appName;
    if (appName == this->app_name_main) {
        // 清理工作
        // this->sct->getApp2AppConnecting(appName);
        // this->sct->getApp2AppStreams(appName);
    } else {
        // 也许得用到重新连接机制
    }
}

void SkycitEngine::onStreamConnectingDone(const QString &appName)
{
    qLogx()<<appName;

    QString contact = this->skygw_name;
    Q_ASSERT(this->app2appStates.contains(appName));
    Q_ASSERT(this->app2appStates.value(appName).contains(contact));
    this->app2appStates[appName][contact].connecting_done_evt = true;
    if (this->app2appStates[appName][contact].use_finished == true
            && this->app2appStates[appName][contact].connecting_done_evt == true) {
        // 可以关闭这个连接了
        this->sct->deleteStream(appName, contact);
    }

    if (appName == this->app_name_main) {

    } else {

    }
}

void SkycitEngine::onStreamData(const QString &appName, const QString &contact)
{
    qLogx()<<appName<<contact;

    QByteArray data = this->sct->readFromStream(appName, contact);

    qLogx()<<appName<<contact<<data;

    if (appName == this->app_name_main) {
        // TODO maybe this data will timeout for some reason
        if (data == "FIN1") {
            this->sct->writeToStream(appName, contact, "FIN2");
            Q_ASSERT(this->app2appStates.contains(appName));
            Q_ASSERT(this->app2appStates.value(appName).contains(contact));
            this->app2appStates[appName][contact].use_finished = true;
            if (this->app2appStates[appName][contact].use_finished == true
                    && this->app2appStates[appName][contact].connecting_done_evt == true) {
                // 可以关闭这个连接了
                this->sct->deleteStream(appName, contact);
            }
            // this->sct->deleteStream(appName, contact); // 如果这时GET APPLICATION CONNECTING不为空，则即使成功关闭，也会自动重连

            // 这时可以连接第二个app2app通道了。
            QHash<QString, A2AStreamUnit> ahash;
            ahash[this->skygw_name] = A2AStreamUnit();
            this->app2appStates[this->app_name_self] = ahash;
            this->sct->newStream(this->app_name_self, this->skygw_name);
        } else {
           Q_ASSERT(1 == 2);
        }
    } else {
        // process app2app ctrl message
        this->onApp2AppMessage(appName, contact, QString(data));
    }
}

void SkycitEngine::onSkypeCallArrived(QString callerName, QString calleeName, int callID)
{
    qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<callerName<<calleeName<<callID;
    // this->m_curr_skype_call_id = callID;
    // this->m_curr_skype_call_peer = calleeName;
}

void SkycitEngine::onSkypeCallHangup(QString contactName, QString callerName, int callID)
{
    qLogx()<<"";
    int skype_call_id = callID;

    // if (callID != this->m_curr_skype_call_id) {
    //     qLogx()<<"Warning:";
    // }
    // this->m_curr_skype_call_id = -1;
    // if (this->m_call_state_widget->isVisible()) {
    //     this->onDynamicSetVisible(this->m_call_state_widget, false);
    // }

    // if (this->wscli.get() && this->wscli->isClosed()) {
    //     qLogx()<<"ws ctrl unexception closed, should cleanup here now.";
    //     this->log_output(LT_USER, "通话异常终止，请重试。");

    //     {
    //         this->onDynamicSetVisible(this->m_call_state_widget, false);
    //         // 关闭网络连续，如果有的话。
    //         this->wscli.reset();
    //         this->uiw->pushButton_4->setEnabled(true);
    //     }
    // }
}

bool SkycitEngine::_addStream(const QString &appName, const QString &contact, int sid)
{
    if (this->app2appConns.contains(appName)) {
        if (this->app2appConns.value(appName).contains(contact)) {
            // not possible, has some problem
            Q_ASSERT(1==2);
        } else {
            this->app2appConns[appName][contact] = sid;
        }
    } else {
        QHash<QString, int> conn;
        conn[contact] = sid;
        this->app2appConns[appName] = conn;
    }
    return false;
}

bool SkycitEngine::_removeStream(const QString &appName, const QString &contact, int sid)
{
    if (this->app2appConns.contains(appName)) {
        if (this->app2appConns.value(appName).contains(contact)) {
            // not possible, has some problem
            this->app2appConns[appName].remove(contact);
        } else {
            Q_ASSERT(1==2);
        }
    } else {
    }
    return false;
}

int SkycitEngine::_getStream(const QString &appName, const QString &contact)
{
    if (this->app2appConns.contains(appName)) {
        if (this->app2appConns.value(appName).contains(contact)) {
            // not possible, has some problem
            return this->app2appConns.value(appName).value(contact);
        } else {
            Q_ASSERT(1==2);
        }
    } else {
    }
    return -1;
}

//////////////////
SkycitEngine::SessionUnit::SessionUnit()
{
    //this->wsi = NULL;
    this->cseq = -1;
    this->acc_id = -1;
}

SkycitEngine::SessionUnit::~SessionUnit()
{

}


///////////////////////////
SCBSignalBridge::SCBSignalBridge(QObject *parent)
    : QThread(parent)
{
    qLogx()<<this->thread();
}

SCBSignalBridge::~SCBSignalBridge()
{

}

// void SCBSignalBridge::run()
// {
//     this->exec();
// }

void SCBSignalBridge::on_websocket_error(int eno)
{
    emit websocket_error(eno);
    qLogx()<<eno;
}

void SCBSignalBridge::on_websocket_started()
{
    emit websocket_started();
    // qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);
    // this->sip_engine_error(eno);
    // emit this->move_runin(boost::bind(&SkycitEngine::on_websocket_started, this));    
    // int seq = ++this->async_seq;
    // boost::function<void()> func = boost::bind(&SkycitEngine::on_websocket_started, this);
    // this->async_funcs[seq] = func;
    // emit this->move_runin(seq);
    
}

// TODO support multi client connect, from desktop app or web app
//void SCBSignalBridge::on_new_websocket_connection()
//{
//    qLogx()<<"";
//    emit new_websocket_connection();

//    // qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);
//    // std::string mvstr;
//    // int cseq = this->mws->nextPendingConnection();
//    // assert(cseq > 0);

//    // if (this->su1.cseq != -1) {
//    //     qLogx()<<"only allow 1 client:"<<this->su1.cseq<<", drop this."<<cseq;
//    // }


//    // mvstr = this->mws->conn_payload_path(cseq);
//    // this->wsconns.left.insert(boost::bimap<int,int>::left_value_type(cseq, cseq));

//    // // TODO check connect validation
//    // this->su1.cseq = cseq;

//    // ////////
//    // this->move_runin(boost::bind(&SkycitEngine::on_send_codec_list, this));
//}

void SCBSignalBridge::on_new_websocket_connection_with(int cseq)
{
    qLogx()<<"";
    emit new_websocket_connection_with(cseq);

    // qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);
    // std::string mvstr;
    // int cseq = this->mws->nextPendingConnection();
    // assert(cseq > 0);

    // if (this->su1.cseq != -1) {
    //     qLogx()<<"only allow 1 client:"<<this->su1.cseq<<", drop this."<<cseq;
    // }


    // mvstr = this->mws->conn_payload_path(cseq);
    // this->wsconns.left.insert(boost::bimap<int,int>::left_value_type(cseq, cseq));

    // // TODO check connect validation
    // this->su1.cseq = cseq;

    // ////////
    // this->move_runin(boost::bind(&SkycitEngine::on_send_codec_list, this));
}

void SCBSignalBridge::on_websocket_message(const std::string &msg, int cseq)
{
    QString qmsg = QString::fromStdString(msg);
    emit websocket_message(qmsg, cseq);
    // qLogx()<<""<<msg.c_str()<<cseq;
    // qLogx()<<QThread::currentThread()<<this<<(QThread::currentThread() == this);

    // if (QThread::currentThread() == this) {
    //     qLogx()<<"got from my wanted thread";
    // } else {
    //     int seq = ++this->async_seq;
    //     boost::function<void()> func = boost::bind(&SkycitEngine::on_websocket_message, this, msg, cseq);
    //     this->async_funcs[seq] = func;
    //     // emit this->move_runin(seq);
    //     this->move_runin_wrapper(this, seq);
    // }

    char tbuf[512] = {0};
    qsnprintf(tbuf, sizeof(tbuf), "Returned by server via %d ,%s", cseq, msg.c_str());
    std::string ret_msg = std::string(tbuf, strlen(tbuf));

    // this->move_runin(boost::bind(&SkycitEngine::process_ctrl_message, this, msg, cseq));

    // for test only
    // this->mws->wssend(cseq, ret_msg);
}

void SCBSignalBridge::on_websocket_connection_closed(int cseq)
{
    // assert(this->su1.cseq == cseq);
    // this->su1.cseq = -1;
    qLogx()<<""<<cseq;
    emit websocket_connection_closed(cseq);
}

