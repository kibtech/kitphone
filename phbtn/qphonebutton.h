/***************************************************************************
 *   Copyright (C) 2008 by P. Sereno                                       *
 *   http://www.sereno-online.com                                          *
 *   http://www.qt4lab.org                                                 *
 *   http://www.qphoton.org                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef QPHONEBUTTON_H
#define QPHONEBUTTON_H

#include <Qt>
#include <QWidget>

#ifdef IN_QPB_PLUGIN
#include <QtDesigner/QDesignerExportWidget>
#else
#undef QDESIGNER_WIDGET_EXPORT
#define QDESIGNER_WIDGET_EXPORT
#endif

class QDESIGNER_WIDGET_EXPORT QPhoneButton : public QWidget
{
 Q_OBJECT
	Q_PROPERTY( QString label READ label WRITE setLabel);
        Q_PROPERTY( QString subLabel READ subLabel WRITE setSubLabel);
	Q_ENUMS( PhoneButtonColor )
	Q_PROPERTY(PhoneButtonColor color READ color WRITE setColor);
			
public: 
    enum PhoneButtonColor {Black,Red,Green,Blue};
    QPhoneButton(QWidget *parent = 0);
    QString label() const { return m_label; }
    QString subLabel() const { return m_subLabel; }
    PhoneButtonColor color() const { return m_color; }
    
public slots:
	void toggleValue();
	void setLabel(QString);
        void setSubLabel(QString);
	QSize sizeHint() { return minimumSizeHint(); }
	QSize minimumSizeHint() { return QSize(50,50); }
	void setColor(PhoneButtonColor);

signals:
/*!
  \brief This signal reports when the switch is toggled
*/
	void valueChanged(QString);
        void clicked();
	
protected:
    bool m_value;
    QString m_label;
    QString m_subLabel;
    PhoneButtonColor m_color;
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *);    
    void mouseReleaseEvent(QMouseEvent *);
    void draw(QPainter *);
};

#endif
