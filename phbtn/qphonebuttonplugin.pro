CONFIG      += designer plugin release
TARGET      = $$qtLibraryTarget($$TARGET)
TEMPLATE    = lib
QTDIR_build:DESTDIR     = $$QT_BUILD_TREE/plugins/designer
DEFINES += IN_QPB_PLUGIN

HEADERS     = qphonebutton.h \
              qphonebuttonplugin.h
SOURCES     = qphonebutton.cpp \
              qphonebuttonplugin.cpp

RESOURCES   = qphonebutton.qrc
# install
target.path = $$[QT_INSTALL_PLUGINS]/designer
sources.files = $$SOURCES $$HEADERS *.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/designer/qphonebuttonplugin
INSTALLS += target sources
