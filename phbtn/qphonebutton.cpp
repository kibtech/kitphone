/***************************************************************************
 *   Copyright (C) 2008 by P. Sereno                                       *
 *   http://www.sereno-online.com                                          *
 *   http://www.qt4lab.org                                                 *
 *   http://www.qphoton.org                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QtGlobal>
#include <QtGui>
#include <QPolygon>


#include "qphonebutton.h"

QPhoneButton::QPhoneButton(QWidget *parent)
    : QWidget(parent)
{
   m_value=false;
   setFocusPolicy(Qt::StrongFocus);
   setMinimumSize(QSize(50,50));
   setCursor(QCursor(Qt::PointingHandCursor));
   m_color=Black;
   
}

void QPhoneButton::paintEvent(QPaintEvent *)
{

	QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    int side = qMin(width(), height());
    painter.setViewport((width() - side) / 2, (height() - side) / 2,side, side);
    painter.setWindow(-50, -50, 100, 100);
    draw(&painter);	
}


void QPhoneButton::draw(QPainter *painter)
{
int xText,yText;
int xStartRect,yStartRect;
QLinearGradient linearGrad;  
QColor c1,c2,c3;
  
   QFont f=font();
   f.setPointSize(20);
   QFont subf = font();
   subf.setPointSize(12);

   QFontMetricsF fm( f );
   double w=fm.size(Qt::TextSingleLine,m_label).width();
   double h=fm.size(Qt::TextSingleLine,m_label).height();
   
   QFontMetrics subfm(subf);
   double subw=subfm.size(Qt::TextSingleLine, m_subLabel).width();
   double subh = subfm.size(Qt::TextSingleLine, m_subLabel).height();

   linearGrad.setStart(QPointF(0, -43));
   linearGrad.setFinalStop(QPointF(0, 43));

   painter->setPen(Qt::NoPen);
   painter->setBrush(Qt::darkGray);        	
   // draw shadow
   painter->drawRoundRect(-43,-43,90,90,25,25);
  
  
 	switch(m_color)
	{
		case Blue:
		   c1=QColor(Qt::darkBlue);
		   c2=QColor(Qt::blue);
		   c3=QColor(Qt::white);
		break;
		case Black:
		   c1=QColor(Qt::black);
		   c2=QColor(Qt::lightGray);
		   c3=QColor(Qt::white);		 
		break;
		case Red:
		   c1=QColor(Qt::darkRed);
		   c2=QColor(Qt::red);
		   c3=QColor(Qt::white);		 		  
		break;
		case Green:
		   c1=QColor(Qt::darkGreen);
		   c2=QColor(Qt::green);
		   c3=QColor(Qt::white);		  
		break;
		default:
		   c1=QColor(Qt::darkBlue);
		   c2=QColor(Qt::blue);
		   c3=QColor(Qt::white);		  
		break;
   }
  
  
   if(!m_value)
   {
  
    linearGrad.setColorAt(0.5, c1);
    linearGrad.setColorAt(0.1, c2);
    linearGrad.setColorAt(0, c3); 	

	xStartRect=-45;
	yStartRect=-45;
	xText=(int)((w/2));
	yText=(int)(h/4);
   }
   else
   {
    linearGrad.setColorAt(0.5, c1);
    linearGrad.setColorAt(0.1,c2);
    linearGrad.setColorAt(0, c3); 
	xStartRect=-43;
	yStartRect=-43;
	xText=(int)((w/2)-3);
	yText=(int)(h/4+3);
   }
   
   // draw Rectangle
   painter->setPen(Qt::NoPen);
   painter->setBrush(linearGrad);        	
   painter->drawRoundRect(xStartRect,yStartRect,90,90,25,25);
   
   // draw Text  
   painter->setPen(Qt::white);
   painter->setBrush(Qt::white);
   QPainterPath path;
   path.addText(QPointF(-xText+20,yText),f,m_label);
   path.addText(QPointF(-xText-25,yText+5), subf, m_subLabel);
   painter->drawPath(path);   
      
}



void QPhoneButton::setLabel(QString value)
{
   m_label=value;
   update();
}

void QPhoneButton::setSubLabel(QString value)
{
   m_subLabel=value;
   update();
}


void QPhoneButton::toggleValue()
{ 
	m_value=!m_value;
	update();
	return; 
}


void QPhoneButton::mousePressEvent(QMouseEvent *)
{
   toggleValue(); 
}


void QPhoneButton::mouseReleaseEvent(QMouseEvent *)
{
   toggleValue();  
   emit valueChanged(m_label);
   emit clicked();
}



void QPhoneButton::setColor(PhoneButtonColor newColor)
{ 
	m_color=newColor;
	update();
	return; 
}
