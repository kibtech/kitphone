#include <QtCore>
#include <QtGui>

namespace Ui {
    class AccountsPanel;
};

class AccountsPanel : public QWidget
{
    Q_OBJECT;
public:
    explicit AccountsPanel(QWidget *parent = 0);
    virtual ~AccountsPanel();

    QSize sizeHint() const;

    bool addAccount(QString name, int type);
    bool removeAccount(QString name, int type);

public slots:
    // void show();
    void onItemClicked(QListWidgetItem *item);

signals:
    void accountSelected(QString name, int type);

protected:
    QVector<QPair<QString,int> > vaccs;
    QHash<QString, int> haccs;
    QMultiHash<QString, int> mhaccs;

private:
    Ui::AccountsPanel *uiw;
    
};
