#!/bin/sh

###################
LOG_DIR=$HOME/skype_cluster/logs

for lf in `ls $LOG_DIR/skyserv_*.log`
do
    echo "Cleaning log $lf ..."
    echo "" > $lf
done

echo "Done." `date`;
