TEMPLATE = app
TARGET = example3
QT -= gui core
CONFIG -= qt
CONFIG += console
DESTDIR = ..
OBJECTS_DIR = ../tmp

win32 {
    INCLUDEPATH += ../../libwss/win32helpers Z:/librarys/vc-zlib/include Z:/cross/boost_1_47_0
    INCLUDEPATH += Z:/librarys/vc-ssl-x86/include
    LIBS += -LZ:/cross/boost_1_47_0/stage/lib -LZ:/librarys/vc-ssl-x86/lib
    LIBS += -llibboost_thread-vc100-mt-s-1_47 -llibboost_serialization-vc100-mt-s-1_47
    LIBS += -lws2_32 # for libwss
} else {
    LIBS += -lboost_thread -lboost_serialization
}

#INCLUDEPATH += .. ../../libev
#DEFINES += EV_CONFIG_H=\\\"ev-config.h\\\"
#SOURCES += ../../libev/ev.c ../../libev/event.c
#win32 {
#    SOURCES += ../../libwss/win32helpers/gettimeofday.c
#}

INCLUDEPATH += ..
include(../src/libcage.pri)
SOURCES += ../example/example3.cpp

win32 {
    LIBS += -llibeay32 -lssleay32 -lws2_32 -lole32 -luser32 -lgdi32 -ladvapi32  #-lssl -lcrypto -lev
} else {
    LIBS += -lssl -lcrypto -Wl,-Bstatic -levent -Wl,-Bdynamic -lrt -lz
}
