// kadim_message_handler.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-05 05:56:09 -0700
// Version: $Id$
// 

#ifndef _KADIM_MESSAGE_HANDLER_H_
#define _KADIM_MESSAGE_HANDLER_H_

#include "maidsafe/transport/transport.h"
#include "maidsafe/dht/message_handler.h"

namespace maidsafe {

class Securifier;

namespace dht {

namespace protobuf {


} // namespace protobuf
} // namespace dht
} // namespace maidsafe


// Highest possible message type ID, use as offset for type extensions.
const int kadimMaxMessageType(maidsafe::dht::kMaxMessageType + 1000);

enum kadimMessageType {
  kKadimMessageRequest = maidsafe::dht::kMaxMessageType + 1,
    kKadimMessageResonse
};

class KadimMessageRequest;
class KadimMessageResponse;

class KadimMessageHandler : public maidsafe::dht::MessageHandler
{
public:
    explicit KadimMessageHandler(std::shared_ptr<maidsafe::Securifier> securifier);
    virtual ~KadimMessageHandler() {}

    std::string WrapMessage(const KadimMessageRequest &msg,
                            const std::string &recipient_public_key);
    std::string WrapMessage(const KadimMessageResponse &msg,
                            const std::string &recipient_public_key);

protected:
    virtual void ProcessSerialisedMessage(const int &message_type,
                                          const std::string &payload,
                                          const maidsafe::SecurityType &security_type,
                                          const std::string &message_signature,
                                          const maidsafe::transport::Info &info,
                                          std::string *message_response,
                                          maidsafe::transport::Timeout *timeout);
    std::string KadimMakeSerialisedWrapperMessage(
            const int &message_type,
            const std::string &payload,
            maidsafe::SecurityType security_type,
            const std::string &recipient_public_key);

private:
    KadimMessageHandler(const KadimMessageHandler&);
    KadimMessageHandler& operator=(const KadimMessageHandler&);

    std::string WrapMessage(const KadimMessageResponse &msg);
};

typedef std::shared_ptr<KadimMessageHandler> KadimMessageHandlerPtr;

#endif /* _KADIM_MESSAGE_HANDLER_H_ */
