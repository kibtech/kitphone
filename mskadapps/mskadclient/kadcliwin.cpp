// kadcliwin.cpp ---
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 22:00:13 +0000
// Version: $Id$
// 

#include "simplelog.h"

#include "kadclient.h"

#include "ui_kadcliwin.h"
#include "kadcliwin.h"

KadCliWin::KadCliWin(QWidget *parent)
    : QMainWindow(parent, 0)
    , uiw(new Ui::KadCliWin())
{
    this->uiw->setupUi(this);

    QObject::connect(this->uiw->pushButton, SIGNAL(clicked()), this, SLOT(onStartKadClient()));
    QObject::connect(this->uiw->pushButton_2, SIGNAL(clicked()), this, SLOT(onSendMessage()));
    QObject::connect(this->uiw->pushButton_4, SIGNAL(clicked()), this, SLOT(onFindKadNode()));
    QObject::connect(this->uiw->pushButton_2, SIGNAL(clicked()), this, SLOT(onSendKadMessage()));
    QObject::connect(this->uiw->pushButton_3, SIGNAL(clicked()), this, SLOT(onStopKadClient()));

    QObject::connect(this->uiw->pushButton_5, SIGNAL(clicked()), this, SLOT(onStoreValue()));
    QObject::connect(this->uiw->pushButton_6, SIGNAL(clicked()), this, SLOT(onFindValue()));
    QObject::connect(this->uiw->pushButton_7, SIGNAL(clicked()), this, SLOT(onDeleteValue()));
}

KadCliWin::~KadCliWin()
{

}

void KadCliWin::onStartKadClient()
{
    std::string idstr;
//    KadClient *kch;

    idstr = this->uiw->lineEdit->text().toStdString();

    kch = new KadClient();
    kch->start(idstr);
}

void KadCliWin::onStopKadClient()
{
    kch->stop();
    // delete kch;
}

void KadCliWin::onFindKadNode()
{
    std::string idstr;

    idstr = this->uiw->lineEdit_2->text().toStdString();

    kch->find_node(idstr);
}

void KadCliWin::onSendKadMessage()
{

}

void KadCliWin::onStoreValue()
{
    std::string skey, sval;

    skey = this->uiw->lineEdit_3->text().toStdString();
    sval = this->uiw->plainTextEdit->toPlainText().toStdString();

    kch->store_value(skey, sval);
}

void KadCliWin::onFindValue()
{
    std::string skey;

    skey = this->uiw->lineEdit_4->text().toStdString();

    kch->find_value(skey);
}

void KadCliWin::onDeleteValue()
{
    std::string skey;
    std::string sval;

    skey = this->uiw->lineEdit_5->text().toStdString();

    kch->delete_value(skey, sval);
}

void KadCliWin::onSendMessage()
{
    qLogx()<<"";
    std::string msg;

    msg = this->uiw->textEdit->toPlainText().toStdString();

    this->kch->send_message(msg);
}
