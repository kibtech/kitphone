#!/bin/sh

set -x

CURR_DIR=`pwd`

cd ../../../msdht-svn/MaidSafe-DHT/src
../../MaidSafe-Common/installed/bin/protoc -oabcde.pb --cpp_out=. kadim.proto
cp -v  kadim.pb.* ../../../kitphone-svn/mskadapps/mskadclient/

cd $CURR_DIR
