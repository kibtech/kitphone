// kadim_message_handler.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-05 05:56:20 -0700
// Version: $Id$
// 

#include "boost/lexical_cast.hpp"
#include "maidsafe/common/securifier.h"

#ifdef __MSVC__
#  pragma warning(push)
#  pragma warning(disable: 4127 4244 4267)
#endif
#include "maidsafe/dht/kademlia.pb.h"
#include "maidsafe/dht/rpcs.pb.h"
#include "kadim.pb.h"
#ifdef __MSVC__
#  pragma warning(pop)
#endif

#include "simplelog.h"
#include "kadim_message_handler.h"

/*
  会报错误了，
libprotobuf ERROR google/protobuf/message_lite.cc:123] Can't parse message of type "maidsafe.transport.protobuf.WrapperMessage" because it is missing required fields: msg_type, payload
I1108 00:00:41.796104  5860 tcp_connection.cc:transport:213] Data size 0 bytes (67108864)

E1108 01:05:12.955211  6490 crypto.cc:common:218] Failed asymmetric decryption: RSA/OAEP-MGF1(SHA-1): invalid ciphertext
W1108 01:05:12.955324  6490 message_handler.cc:transport:63] Failed to decrypt: encrypt_aes_seed is empty.
I1108 01:05:12.955358  6490 tcp_connection.cc:transport:213] Data size 0 bytes (67108864)


  */

KadimMessageHandler::KadimMessageHandler(std::shared_ptr<maidsafe::Securifier> securifier)
    : maidsafe::dht::MessageHandler(securifier)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<std::endl;
    qLogx()<<"";
}

std::string KadimMessageHandler::WrapMessage(const KadimMessageRequest &msg,
                        const std::string &recipient_public_key)
{
    qLogx()<<"";
    std::string str;

    str = "hahahhhhhhhhhhhhhhhhhhhh";

    if (!msg.IsInitialized())
//      return "";
        return str;
    qLogx()<<"";
    return MakeSerialisedWrapperMessage(kKadimMessageRequest, msg.SerializeAsString(),
                                        maidsafe::kAsymmetricEncrypt, recipient_public_key);

    return str;
}

std::string KadimMessageHandler::WrapMessage(const KadimMessageResponse &msg,
                        const std::string &recipient_public_key)
{
    qLogx()<<"";
    std::string str;

    str = "hahahhhhhhhhhhhhhhhhhhhh";

    if (!msg.IsInitialized())
//      return "";
        return str;
    qLogx()<<"";
    return MakeSerialisedWrapperMessage(kKadimMessageResonse, msg.SerializeAsString(),
                                        maidsafe::kAsymmetricEncrypt, recipient_public_key);

    return str;
}

void KadimMessageHandler::ProcessSerialisedMessage(const int &message_type,
                                      const std::string &payload,
                                      const maidsafe::SecurityType &security_type,
                                      const std::string &message_signature,
                                      const maidsafe::transport::Info &info,
                                      std::string *message_response,
                                      maidsafe::transport::Timeout *timeout)
{
    std::cout<<"hhaaaaaaaaaaaaaaaa...."<<message_type<<payload<<std::endl;
    switch (message_type) {
    case kKadimMessageRequest: {
        qLogx()<<"";
        if (security_type != maidsafe::kAsymmetricEncrypt)
            return;
        KadimMessageRequest request;
        // protobuf::FindValueRequest request;
        if (request.ParseFromString(payload) && request.IsInitialized()) {
            KadimMessageResponse response;
            // (*on_find_value_request_)(info, request, &response, timeout);
            *message_response = WrapMessage(response,
                                            request.sender().public_key());
        }
        break;
    }
    case kKadimMessageResonse: {
        qLogx()<<"";

        if (security_type != maidsafe::kAsymmetricEncrypt)
            return;
        KadimMessageResponse response;
        if (response.ParseFromString(payload) && response.IsInitialized()) {
            // (*on_find_value_response_)(info, response);
        }
        break;
    }
    default:
        maidsafe::dht::MessageHandler::ProcessSerialisedMessage(message_type,
                                                                payload,
                                                                security_type,
                                                                message_signature,
                                                                info,
                                                                message_response,
                                                                timeout);
        break;
    }
}

std::string KadimMessageHandler::KadimMakeSerialisedWrapperMessage(
        const int &message_type,
        const std::string &payload,
        maidsafe::SecurityType security_type,
        const std::string &recipient_public_key)
{
    qLogx()<<"";

    switch (message_type) {
    case kKadimMessageRequest:
        qLogx()<<"";
        return maidsafe::dht::MessageHandler::MakeSerialisedWrapperMessage(message_type,
                                                                           payload,
                                                                           security_type,
                                                                           recipient_public_key);
        break;
    case kKadimMessageResonse:
        qLogx()<<"";
        return maidsafe::dht::MessageHandler::MakeSerialisedWrapperMessage(message_type,
                                                                           payload,
                                                                           security_type,
                                                                           recipient_public_key);
        break;
    default:
        return maidsafe::dht::MessageHandler::MakeSerialisedWrapperMessage(message_type,
                                                                           payload,
                                                                           security_type,
                                                                           recipient_public_key);
        break;
    }

    return std::string();
}
