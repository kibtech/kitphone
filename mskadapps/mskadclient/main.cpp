// main.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 22:04:10 +0000
// Version: $Id$
// 

#include <QtGui>

#include "kadcliwin.h"

int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    KadCliWin kcw;

    kcw.show();

    return a.exec();
    return 0;
}
