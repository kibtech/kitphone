// kadcliwin.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 21:59:23 +0000
// Version: $Id$
// 

#ifndef _KADCLIWIN_H_
#define _KADCLIWIN_H_


#include <QtGui>

class KadClient;

namespace Ui {
    class KadCliWin;
}

class KadCliWin : public QMainWindow
{
    Q_OBJECT;
public:
    KadCliWin(QWidget *parent = 0);
    virtual ~KadCliWin();

public slots:
    void onStartKadClient();
    void onStopKadClient();

    void onFindKadNode();
    void onSendKadMessage();

    void onStoreValue();
    void onFindValue();
    void onDeleteValue();

    void onSendMessage();

private:
    Ui::KadCliWin *uiw;
    KadClient *kch;
    QHash<QString, QString> deleting_list;
};

#endif /* _KADCLIWIN_H_ */
