TEMPLATE = app
TARGET = kadclient
# CONFIG -= qt
QT = core gui



include(../mskad_qmconfig.pri)


















#QMAKE_CC = clang
#QMAKE_CXX = clang++
#QMAKE_CXXFLAGS += -std=c++0x -g

#INCLUDEPATH += ../../../msdht-svn/MaidSafe-Common/installed/include
#LIBS += -L../../../msdht-svn/MaidSafe-Common/installed/lib/ -lmaidsafe_dht  -lmaidsafe_transport -lmaidsafe_common -lcryptopp -lglog -lprotobuf

#LIBS += -lboost_system -lboost_thread -lboost_filesystem -lboost_date_time -lboost_serialization

INCLUDEPATH += ../mskadserv/ ../../

# protoc -okadim.pb --cpp_out=. kadim.proto

SOURCES += main.cpp kadclient.cpp kadcliwin.cpp ../mskadserv/nodeconst.cpp ../mskadserv/nodefunctor.cpp \
        node_container_cex.cpp kadim.pb.cc kadim_message_handler.cpp
HEADERS += kadclient.h kadcliwin.h ../mskadserv/nodeconst.h  ../mskadserv/nodefunctor.h \
        node_container_cex.h kadim.pb.h kadim_message_handler.h
FORMS += kadcliwin.ui

SOURCES += ../../simplelog.cpp
HEADERS += ../../simplelog.h
