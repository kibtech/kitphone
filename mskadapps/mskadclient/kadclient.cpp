// kadclient.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 22:00:38 +0000
// Version: $Id$
// 

#include <stdlib.h>
#include <stdio.h>

#include <assert.h>
#include <exception>
#include <signal.h>

#include "simplelog.h"

#include "boost/filesystem.hpp"
#include "boost/program_options.hpp"
#include "boost/archive/xml_iarchive.hpp"
#include "boost/archive/xml_oarchive.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include <boost/thread/condition_variable.hpp>

// #include "maidsafe/common/breakpad.h"

 #include "maidsafe/dht/log.h"
//#include "maidsafe/transport/udp_transport.h"
//#include "maidsafe/transport/tcp_transport.h"
//#include "maidsafe/dht/version.h"
//#include "maidsafe/dht/contact.h"
//#include "maidsafe/dht/return_codes.h"
//#include "maidsafe/dht/node_id.h"
//#include "maidsafe/dht/node-api.h"
//#include "maidsafe/dht/node_container.h"
#include "maidsafe/dht/message_handler.h"
// #include "maidsafe/dht/demo/commands.h"
#include "maidsafe/dht/utils.h"

#include "kadim.pb.h"

#include "kadim_message_handler.h"
#include "kadclient.h"
#include "nodefunctor.h"
#include "nodeconst.h"

namespace bptime = boost::posix_time;
namespace fs = boost::filesystem;
namespace bfs3 = boost::filesystem3;
namespace po = boost::program_options;
namespace mk = maidsafe::dht;
namespace mt = maidsafe::transport;

const char *client_node_contact_file = "./kitech_client_node_contact.xml";

KadClient::KadClient()
    : QObject(0)
{
    this->ncb.joined.connect(boost::bind(&KadClient::onNodeJoined, this, _1));
    this->ncb.valueFound.connect(boost::bind(&KadClient::onValueFound, this, _1));
    this->ncb.nodeFound.connect(boost::bind(&KadClient::onNodeFound, this, _1, _2));
    this->ncb.contactFound.connect(boost::bind(&KadClient::onContactFound, this, _1, _2));
}

KadClient::~KadClient()
{

}

bool KadClient::WriteBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                                   const std::string &filename) {
    try {
        std::ofstream ofs(filename);
        boost::archive::xml_oarchive oa(ofs);
        boost::serialization::serialize(oa, *bootstrap_contacts, 0);
        // DLOG(INFO) << "Updated bootstrap info.";
    } catch(const std::exception &e) {
        // DLOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    return true;
}

bool KadClient::ReadBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                                  const std::string &filename) {
    try {
        std::ifstream ifs(filename);
        boost::archive::xml_iarchive ia(ifs);
        boost::serialization::serialize(ia, *bootstrap_contacts, 0);
    } catch(const std::exception &e) {
        // DLOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    return true;
}

std::string KadClient::rewrite_nodeid(const std::string &skey)
{
    std::string rkey, rkey2;

    rkey = skey;
    rkey.append("$");
    for (int i = 0; i < mk::kKeySizeBytes - skey.length()-1; i++) {
        rkey.append("0");
    }
    rkey2 = maidsafe::EncodeToHex(rkey);

    std::cout<<"rewrited key length:"<<rkey<<rkey.length()<<rkey2<<std::endl;

    return rkey2;
}

std::string KadClient::restore_nodeid(const std::string &ekey)
{
    std::string skey;
    char dch = '$';

    skey = maidsafe::DecodeFromHex(ekey);
    skey = skey.substr(0, skey.find_first_of(dch));

    std::cout<<"restore key:"<<skey.length()<<skey<<std::endl;
    return skey;
}

mk::SecurifierPtr KadClient::genateSecurifier(const  mk::Contact &contact, const char *rootnode_id, bool newkey)
{
    mk::SecurifierPtr nsec;
    std::string pubkey, privkey;
    mk::NodeId nid(rewrite_nodeid(rootnode_id), mk::NodeId::kHex);

    if (!newkey) {
        std::ifstream nki(std::string("./keys/") + std::string(rootnode_id) + ".pubkey");
        nki >> pubkey;
        std::ifstream nki2(std::string("./keys/") + std::string(rootnode_id) + ".privkey");
        nki2 >> privkey;
        nsec.reset(new maidsafe::Securifier(nid.String(), maidsafe::DecodeFromBase64(pubkey), maidsafe::DecodeFromBase64(privkey)));
    } else {
        maidsafe::crypto::RsaKeyPair key_pair;
        do {
            maidsafe::Sleep(boost::posix_time::milliseconds(50));
            key_pair.GenerateKeys(4096);
            nsec.reset(new maidsafe::Securifier(nid.String(), key_pair.public_key(), key_pair.private_key()));
        } while (key_pair.public_key().empty() || key_pair.private_key().empty());
        std::ofstream nko(std::string("./keys/") + std::string(rootnode_id) + ".pubkey");
        nko << maidsafe::EncodeToBase64(key_pair.public_key());
        std::ofstream nko2(std::string("./keys/") + std::string(rootnode_id) + ".privkey");
        nko2 << maidsafe::EncodeToBase64(key_pair.private_key());
    }

    return nsec;
}

bool KadClient::start(const std::string &idstr)
{
    this->midstr = idstr;
    int iret;
    mk::Contact contact, contact2;
    mk::NodeContainerCex<mk::Node> *nnc;
//    mk::MessageHandlerPtr nmh;
//    KadimMessageHandlerPtr nmh2;
    mk::SecurifierPtr nsec;
    mk::AlternativeStorePtr nstore;
    bool client_only = false;

    join_fh = std::bind(&NodeFunctor::joinCallback, &ncb, std::placeholders::_1);
    store_fh = std::bind(&NodeFunctor::storeCallback, &ncb, std::placeholders::_1);
    delete_fh = std::bind(&NodeFunctor::deleteCallback, &ncb, std::placeholders::_1);
    find_fh = std::bind(&NodeFunctor::findCallback, &ncb, std::placeholders::_1);
    lookup_fh = std::bind(&NodeFunctor::lookupCallback, &ncb, std::placeholders::_1, std::placeholders::_2);
    getctt_fh = std::bind(&NodeFunctor::getContactCallback, &ncb, std::placeholders::_1, std::placeholders::_2);

    std::pair<mt::Port, mt::Port> port_range(port_base, port_base + port_range_size);

    this->rncs.clear();
    if (boost::filesystem3::exists(boost::filesystem3::path(rootnode_contacts_file))) {
        this->ReadBootstrapFile(&this->rncs, std::string(rootnode_contacts_file));
    } else {
        assert(1==2);
    }
    assert(this->rncs.size() > 0);

    for (int i = 0; i < 1; ++i) {
        nnc = new mk::NodeContainerCex<mk::Node>();
        nncs.push_back(nnc);

        nnc->set_join_functor(join_fh);
        nnc->set_store_functor(store_fh);
        nnc->set_delete_functor(delete_fh);
        nnc->set_find_value_functor(find_fh);
        nnc->set_find_nodes_functor(lookup_fh);
        nnc->set_get_contact_functor(getctt_fh);

        contact = mk::Contact();
        if (0 && rncs.size() > 0) {
            contact = rncs.at(i);
            nsec = this->genateSecurifier(contact, midstr.c_str(), false);
//            nsec = this->genateSecurifier(contact, rootnode_ids[i], false);
        } else {
            // 自动按照规则生成一批rootnode,并保存到该文件中
            nsec = this->genateSecurifier(contact, midstr.c_str(), true);
        }

        this->message_handler_.reset(new KadimMessageHandler(nsec));
        // nnc->Init(3, nsec, nmh, nstore, client_only);
        nnc->Init(3, nsec, this->message_handler_, nstore, client_only);

        // 这个server有问题，在关闭后立即启动会出错。
        iret = nnc->Start(this->rncs, port_range);
        if (iret == 0) {
            contact2 = nnc->node()->contact();
            rncs2.push_back(contact2);
            std::cout<<i<<" OK "<<iret<<std::endl;
        } else {
            std::cout<<i<<" ERR "<<iret<<std::endl;
        }
    }

//    if (rncs.size() == 0) {
//        this->WriteBootstrapFile(&rncs2, std::string(rootnode_contacts_file));
//    }

    return true;
}

bool KadClient::stop()
{
    mk::NodeContainerCex<mk::Node> *nnc;
    nnc = this->nncs.at(0);

    int iret = nnc->Stop(&this->rncs);

    qLogx()<<"stop node:"<<iret<<this->rncs.size();

    this->nncs.clear();

    delete nnc;

    return true;
}


bool KadClient::find_node(const std::string &fkey)
{
    mk::NodeContainerCex<mk::Node> *nnc;
    mk::NodeId aid(this->rewrite_nodeid(fkey), mk::NodeId::kHex);

    nnc = this->nncs.at(0);

    nnc->GetContact(aid);

    return true;
}

// skey store key in human natural format
bool KadClient::find_value(const std::string &skey)
{
    mk::NodeContainerCex<mk::Node> *nnc;
    nnc = this->nncs.at(0);

    //
    mk::NodeId knid(this->rewrite_nodeid(skey), mk::NodeId::kHex);
    mk::SecurifierPtr nsec = nnc->securifier();

    nnc->FindValue(knid, nsec);

    return true;
}

// skey store key in human natural format, sval stored value in human natural format
bool KadClient::store_value(const std::string &skey, const std::string &sval)
{
    mk::NodeContainerCex<mk::Node> *nnc;
    nnc = this->nncs.at(0);

    mk::NodeId knid(this->rewrite_nodeid(skey), mk::NodeId::kHex);
    std::string signature;
    boost::posix_time::time_duration ttl(0,60,0);
    mk::SecurifierPtr nsec = nnc->securifier();

    nnc->Store(knid, sval, signature, ttl, nsec);

    return true;
}

bool KadClient::delete_value(const std::string &skey, const std::string &sval)
{
    mk::NodeContainerCex<mk::Node> *nnc;
    nnc = this->nncs.at(0);

    mk::NodeId knid(this->rewrite_nodeid(skey), mk::NodeId::kHex);
    std::string signature;
    // boost::posix_time::time_duration ttl(0,60,0);
    mk::SecurifierPtr nsec = nnc->securifier();

    nnc->Delete(knid, sval, signature, nsec);

    return true;
}

bool KadClient::send_message(const std::string &msg)
{
    qLogx()<<msg.c_str();

    maidsafe::transport::Endpoint pe = this->last_find_node.endpoint();
    qLogx()<<pe.ip.to_string().c_str()<<pe.port;

    mk::NodeContainerCex<mk::Node> *nnc;
    nnc = this->nncs.at(0);

    //    std::string tmsg = "hahaaaaaaaaaaaaaaaaaaaa";
    mk::SecurifierPtr securifer = nnc->securifier();
    securifer->kSigningPublicKey();
    std::string tmsg = msg;
    bptime::time_duration duto(0, 0, 5);
    mk::TransportPtr tp = nnc->transporter();
    // tp->Send(tmsg, pe, duto);

    // TODO send via router
    {
        mk::TransportPtr transport = nnc->transporter();
        KadimMessageHandlerPtr message_handler = nnc->messager_handler();
        // KadimMessageHandler *message_handler = (KadimMessageHandler*)nnc->messager_handler().get();
        // Prepare(securifier, transport, message_handler);
//        uint32_t object_indx =
//            connected_objects_.AddObject(transport, message_handler);

        KadimMessageRequest request;
        request.set_imsg(msg);
        *request.mutable_sender()  = mk::ToProtobuf(nnc->node()->contact());
        *request.mutable_reciever() = mk::ToProtobuf(this->last_find_node);
        // *request.mutable_sender() = PrepareContact(securifier);
//        std::string random_data(RandomString(50 + (RandomUint32() % 50)));
//        request.set_ping(random_data);
//        std::shared_ptr<RpcsFailurePeer> rpcs_failure_peer(new RpcsFailurePeer);
//        rpcs_failure_peer->peer = peer;
        std::string message(message_handler->WrapMessage(request, this->last_find_node.public_key()));

        // Connect callback to message handler for incoming parsed response or error
//        message_handler->on_ping_response()->connect(
//            std::bind(&Rpcs::PingCallback, this, random_data, transport::kSuccess,
//                      arg::_1, arg::_2, object_indx, callback, message,
//                      rpcs_failure_peer));
//        message_handler->on_error()->connect(
//            std::bind(&Rpcs::PingCallback, this, random_data, arg::_1,
//                      transport::Info(), protobuf::PingResponse(), object_indx,
//                      callback, message, rpcs_failure_peer));
        // DLOG(INFO) << "\t2 " << DebugId(contact_) << " PING to " << DebugId(peer);
        transport->Send(message,
                        pe,
                        maidsafe::transport::kDefaultInitialTimeout);
    }


    return true;
}

void KadClient::onNodeJoined(int result)
{
    qLogx()<<result;
}

void KadClient::onValueStored(int result)
{
    qLogx()<<result;
}

void KadClient::onValueDeleted(int result)
{
    qLogx()<<result;
}

void KadClient::onValueFound(maidsafe::dht::FindValueReturns rets)
{
    qLogx()<<rets.return_code;
}

void KadClient::onNodeFound(int result, std::vector<maidsafe::dht::Contact> contacts)
{
    qLogx()<<result;
}

void KadClient::onContactFound(int result, maidsafe::dht::Contact contact)
{
    qLogx()<<result;

    this->last_find_node = contact;

//    maidsafe::transport::Endpoint pe = contact.endpoint();
//    qLogx()<<pe.ip.to_string().c_str()<<pe.port;

//    mk::NodeContainerCex<mk::Node> *nnc;
//    nnc = this->nncs.at(0);

//    std::string tmsg = "hahaaaaaaaaaaaaaaaaaaaa";
//    bptime::time_duration duto(0, 0, 5);
//    mk::TransportPtr tp = nnc->transporter();
//    tp->Send(tmsg, pe, duto);
}
