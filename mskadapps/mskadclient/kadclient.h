// kadclient.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 22:00:26 +0000
// Version: $Id$
// 

#ifndef _KADCLIENT_H_
#define _KADCLIENT_H_

#include <string>

#include <QtCore>

#include <boost/signals2.hpp>

#include "maidsafe/common/crypto.h"
#include "maidsafe/common/utils.h"
#include "maidsafe/common/securifier.h"

#include "maidsafe/dht/version.h"
#include "maidsafe/dht/contact.h"
#include "maidsafe/dht/return_codes.h"
#include "maidsafe/dht/node_id.h"
#include "maidsafe/dht/node-api.h"
//#include "maidsafe/dht/node_container.h"
#include "maidsafe/dht/contact.h"

#include "node_container_cex.h"

#include "nodefunctor.h"

class KadimMessageHandler;

class KadClient : public QObject
{
    Q_OBJECT;
public:
    explicit KadClient();
    virtual ~KadClient();

    bool start(const std::string &idstr);
    bool stop();

    static bool WriteBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                            const std::string &filename) ;
    static bool ReadBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                           const std::string &filename);

    // 把任意长度的key格式化为mskad需要的64字节固定长度的值，格式：raw_id_str$000000.....
    // 返回一个格式化字符串的HEX值，长度必为128
    static std::string rewrite_nodeid(const std::string &skey);
    // 把一个key的HEX值还原为格式化字符串，并返回原串raw_id_str，其余的冗余部分被清除掉。
    static std::string restore_nodeid(const std::string &ekey);

    // fkey node id in human natural format
    bool find_node(const std::string &fkey);

    // skey store key in human natural format
    bool find_value(const std::string &skey);

    // skey store key in human natural format, sval stored value in human natural format
    bool store_value(const std::string &skey, const std::string &sval);

    bool delete_value(const std::string &skey, const std::string &sval);

    bool send_message(const std::string &msg);

    // boost slot
    /*
    boost::signals2::signal<void(int)> joined;
    boost::signals2::signal<void(int)> stored;
    boost::signals2::signal<void(int)> deleted;
    boost::signals2::signal<void(maidsafe::dht::FindValueReturns)> valueFound;
    boost::signals2::signal<void(int, std::vector<maidsafe::dht::Contact>)> nodeFound;
    boost::signals2::signal<void(int, maidsafe::dht::Contact)> contactFound;
      */
    void onNodeJoined(int result);
    void onValueStored(int result);
    void onValueDeleted(int result);
    void onValueFound(maidsafe::dht::FindValueReturns rets);
    void onNodeFound(int result, std::vector<maidsafe::dht::Contact> contacts);
    void onContactFound(int result, maidsafe::dht::Contact contact);

    // boost::signals2::signal<void(int)>::slot_type onNodeJoined(int result);

signals:
    void joined(int result);
    void stored(int result);
    void deleted(int result);


protected:
    mk::SecurifierPtr genateSecurifier(const  mk::Contact &contact, const char *rootnode_id, bool newkey);

protected:
    NodeFunctor ncb;
    std::shared_ptr<KadimMessageHandler> message_handler_;
    std::vector<mk::Contact> rncs;
    std::vector<mk::Contact> rncs2;
    std::vector<mk::NodeContainerCex<mk::Node> *> nncs;
    static const int port_base = 18500;
    static const int root_node_count = 8;
    static const int port_range_size = 20000;

    std::function<void(int)> join_fh;
    std::function<void(int)> store_fh;
    std::function<void(int)> delete_fh;
    std::function<void(mk::FindValueReturns)> find_fh;
    std::function<void(int, std::vector<mk::Contact>)> lookup_fh;
    std::function<void(int, mk::Contact)> getctt_fh;

    std::string midstr;

    mk::Contact last_find_node;
};

#endif /* _KADCLIENT_H_ */
