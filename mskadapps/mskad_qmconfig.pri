#QMAKE_CC = clang
#QMAKE_CXX = clang++
*-clang {
    QMAKE_CXXFLAGS += -v -std=c++0x -g
} else {
    QMAKE_CXXFLAGS += -std=c++0x -g
}
# INCLUDEPATH += /usr/include/c++/4.6.2/x86_64-unknown-linux-gnu/
# INCLUDEPATH += /usr/include/c++/$$system("gcc -dumpversion")/$$system("gcc -dumpmachine")/

INCLUDEPATH += $$PWD/../../msdht-svn/MaidSafe-Common/installed/include
LIBS += -L$$PWD/../../msdht-svn/MaidSafe-Common/installed/lib/ -lmaidsafe_dht  -lmaidsafe_transport -lmaidsafe_common -lcryptopp -lglog -lprotobuf

INCLUDEPATH += $$PWD/../../boost_1_47_0
LIBS += -L$$PWD/../../boost_1_47_0/stage/lib
LIBS += -lboost_system -lboost_thread -lboost_filesystem -lboost_date_time -lboost_serialization -lpthread
