#include <stdio.h>
#include <stdlib.h>

#include <exception>

#include <signal.h>
#include "boost/filesystem.hpp"
#include "boost/program_options.hpp"
#include "boost/archive/xml_iarchive.hpp"
#include "boost/archive/xml_oarchive.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include <boost/thread/condition_variable.hpp>


#include "maidsafe/common/crypto.h"
#include "maidsafe/common/utils.h"
#include "maidsafe/common/securifier.h"
// #include "maidsafe/common/breakpad.h"

// #include "maidsafe/dht/log.h"
#include "maidsafe/transport/udp_transport.h"
#include "maidsafe/transport/tcp_transport.h"
#include "maidsafe/dht/version.h"
#include "maidsafe/dht/contact.h"
#include "maidsafe/dht/return_codes.h"
#include "maidsafe/dht/node_id.h"
#include "maidsafe/dht/node-api.h"
#include "maidsafe/dht/node_container.h"
#include "maidsafe/dht/message_handler.h"
// #include "maidsafe/dht/demo/commands.h"


namespace bptime = boost::posix_time;
namespace fs = boost::filesystem;
namespace po = boost::program_options;
namespace mk = maidsafe::dht;
namespace mt = maidsafe::transport;

bool WriteBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                        const std::string &filename) {
  try {
    std::ofstream ofs(filename);
    boost::archive::xml_oarchive oa(ofs);
    boost::serialization::serialize(oa, *bootstrap_contacts, 0);
    // DLOG(INFO) << "Updated bootstrap info.";
  } catch(const std::exception &e) {
    // DLOG(WARNING) << "Exception: " << e.what();
    return false;
  }
  return true;
}

bool ReadBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                       const std::string &filename) {
  try {
    std::ifstream ifs(filename);
    boost::archive::xml_iarchive ia(ifs);
    boost::serialization::serialize(ia, *bootstrap_contacts, 0);
  } catch(const std::exception &e) {
    // DLOG(WARNING) << "Exception: " << e.what();
    return false;
  }
  return true;
}

struct StoreResultor {
    void operator ()(int result) {
        std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<std::endl;
    }

};

struct FindVor {
    void operator() (mk::FindValueReturns rets) {
        std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<rets.return_code<<std::endl;
    }
};

struct LookupVor {
    void operator()(int result , std::vector<mk::Contact> contacts) {
         std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<" cts:"<< contacts.size()<<std::endl;
    }
};

class OpResultHandler
{
public:
    OpResultHandler() {}
    ~OpResultHandler() {}

    void storeRF(int result) {
         std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<std::endl;
    }
    void FindRF(mk::FindValueReturns rets) {
        std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<rets.return_code<<std::endl;
    }

    void LoopUpRF(int result , std::vector<mk::Contact> contacts) {
         std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<" cts:"<< contacts.size()<<std::endl;
         for (int i = 0; i < contacts.size(); ++i) {
             std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"idx:"<<i<<mk::DebugId(contacts.at(i).node_id())<<std::endl;
         }
    }
};



std::string rewritekey(std::string skey)
{
    std::string rkey, rkey2;
    char ch1, ch2;
    const char *hex_chars = "0123456789abcdef";
    char tmp[3] = {0};

    rkey = skey;

    for (int i = 0; i < mk::kKeySizeBytes - skey.length(); i++) {
        rkey.append("0");
    }


    for (int i = 0; i < rkey.length() ; ++i) {
        ch1 = (rkey.at(i) >> 4) & 0xf0;
        ch2 = (rkey.at(i)) & 0x0f;

        tmp[0] = hex_chars[ch1];
        tmp[1] = hex_chars[ch2];
        rkey2.append(tmp);
    }

    std::cout<<"rewrited key length:"<<rkey<<rkey.length()<<rkey2<<std::endl;

    return rkey2;
}

/*
  rootnode, supernode, clientnode
  */
/*
  ./demo f
  */
int main(int argc, char **argv)
{
    bool bret;
    int iret;
    boost::mutex nmtx;
    boost::condition_variable ncondvar;

    OpResultHandler opr;
    mk::Contact contact, contact2;
    std::vector<mk::Contact>  contacts, contacts2;
    mk::AsioService asvc;
    uint16_t nk, nalpha, nbeta;
    boost::posix_time::time_duration ntd;
    boost::asio::io_service bios;
    std::pair<mt::Port, mt::Port> port_range(5600, 5604);
    // maidsafe::transport::UdpTransport *_autp;
    mk::TransportPtr ntptr(new maidsafe::transport::UdpTransport(asvc));
    mk::MessageHandlerPtr nmh;
    mk::SecurifierPtr nsec;
    mk::AlternativeStorePtr nstore;
    bool client_only =  argc == 2 ? false : true;
    mk::Node anode(asvc, ntptr, nmh, nsec, nstore, client_only, nk, nalpha, nbeta, ntd);
    mk::NodeContainer<mk::Node> nnc;


//    std::function<void(int)> store_fun = std::bind(&OpResultHandler::storeRF, &opr, 123);
//    std::function<void(mk::FindValueReturns)> find_fun =
//            std::bind(&OpResultHandler::FindRF, &opr, mk::FindValueReturns());
    std::function<void(int)> store_fun = StoreResultor();
    std::function<void(mk::FindValueReturns)> find_fun = FindVor();
    std::function<void(int,std::vector<mk::Contact>)> loopup_fun = //LookupVor();
            std::bind(&OpResultHandler::LoopUpRF, &opr, std::placeholders::_1, std::placeholders::_2);

    nnc.set_store_functor(store_fun);
    nnc.set_find_value_functor(find_fun);
    nnc.set_join_functor(store_fun); // 如果没有的话，会导致程序崩溃
    nnc.set_find_nodes_functor(loopup_fun); // 如果没有的话，会导致程序崩溃
//    // store_fun(123);
//    nnc.MakeAllCallbackFunctors(&nmtx, &ncondvar);

//    contact = nnc.node()->contact();
//    std::cout<<"node:"<<mk::DebugId(contact.node_id())<<std::endl;

    nnc.Init(3, nsec, nmh, nstore, client_only);
    contact = nnc.node()->contact();
    std::cout<<"node:"<<mk::DebugId(contact.node_id())<<std::endl;


    if (argc == 2) {
        // server mode
        iret = nnc.Start(contacts, port_range);
        std::cout<<iret<<std::endl;
    } else {
        contacts.clear();

        bret = ::ReadBootstrapFile(&contacts, "abcd.xml");
        std::cout<<"client boot nodes:"<<contacts.size()<<std::endl;

        iret = nnc.StartClient(contacts);
        // iret = nnc.Start(contacts, port_range);
        std::cout<<"client: "<<iret<<std::endl;


        mk::NodeId rid("c447d763e98f0313912642ff3f066e9a3bc6e60f110d6c8a21cef43e3678f62636f9c7f5a7be2e238cf1a65f50ea8329a1a32b66853248220b655dbf865465f7");
        // nnc.Join(rid, contacts);

    }
    // anode.Join();

    // contacts.push_back(contact);
    // contacts.push_back(contact2);

    contact = nnc.node()->contact();
    std::cout<<"node:"<<mk::DebugId(contact.node_id())<<std::endl;

    contacts2 = nnc.bootstrap_contacts();
    contacts2.push_back(contact);
    if (argc == 2) {
        // contacts2.push_back(contact);
        bret = ::WriteBootstrapFile(&contacts2, "abcd.xml");
    }

    // bret = ::ReadBootstrapFile(&contacts2, "abcd.xml");
    std::cout<<contacts.size()<< contacts2.size()<<std::endl;

    std::string stval = contact.node_id().ToStringEncoded(mk::NodeId::kBase64);
    std::string stkey, fstkey;
    fstkey = "stid_liuguangzhao987654321";
    if (argc == 2) {
        stkey = "stid_liuguangzhao987654321";
    } else {
        stkey = "stid_drswinghead1233dsfdsf";
    }
    stkey = rewritekey(stkey);
    fstkey = rewritekey(fstkey);

    mk::Key skey(stkey, (mk::NodeId::EncodingType)mk::NodeId::kHex);
    mk::Key fskey(fstkey, (mk::NodeId::EncodingType)mk::NodeId::kHex);

    std::cout<<"Storing .."<<stkey<<" -> "<<stval<<skey.IsValid()<<fskey.IsValid()<<std::endl;


    nnc.Store(skey, stval, std::string(),
              boost::posix_time::time_duration(1, 0, 0, 0), mk::SecurifierPtr());

    nnc.FindValue(fskey, mk::SecurifierPtr());

    nnc.FindNodes(fskey);


//    while (1) {
//        // boost::unique_lock<boost::mutex> lock(nmtx);
//        boost::mutex::scoped_lock lock(nmtx);
//        ncondvar.wait(lock);
//        // DLOG(INFO)<<"aaaaaaaaaa";
//        std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<std::endl;
//    }

    std::cout<<"sleeping... for svc running severval seconds."<<std::endl;
    boost::this_thread::sleep(boost::posix_time::millisec(50000));
    boost::this_thread::sleep(boost::posix_time::seconds(3));
    boost::this_thread::sleep(boost::posix_time::seconds(3000));
    return 0;
}
