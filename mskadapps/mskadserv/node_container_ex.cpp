#include "node_container_ex.h"

namespace maidsafe {

namespace dht {

std::string getLocalOuterAddress()
{
    std::string addr, addr2;

    std::vector<transport::IP> ips = transport::GetLocalAddresses();

//    std::cout<<"ipcount:"<<ips.size()<<std::endl;
    for (int i = 0; i < ips.size(); ++i) {
        addr2 = ips.at(i).to_string();

//        std::cout<<i<<"ip:"<<addr2
//                <<" "<<(addr2.find("127.") == 0)
//               <<" "<<(addr2.find("10.") == 0)
//              <<" "<<(addr2.find("192.") == 0)
//             <<" "<<(addr2.find("172.") == 0)
//            <<std::endl;

        if (addr2.find("127.") == 0) {
            // loopback
        } else if (addr2.find("10.") == 0) {
            //

        } else if (addr2.find("192.") == 0) {

        } else if (addr2.find("172.") == 0) {

        } else {
            // break;
            addr = addr2;
            return addr;
        }
    }

    for (int i = 0; i < ips.size(); ++i) {
        addr2 = ips.at(i).to_string();

        if (addr2.find("127.") == 0) {
            // loopback
        } else if (addr2.find("10.") == 0) {
            //

        } else if (addr2.find("192.") == 0) {
            addr = addr2;
            return addr;
        } else if (addr2.find("172.") == 0) {

        } else {
            // break;
            // addr = addr2;
        }
    }

    for (int i = 0; i < ips.size(); ++i) {
        addr2 = ips.at(i).to_string();

        if (addr2.find("127.") == 0) {
            // loopback
        } else if (addr2.find("10.") == 0) {
            //

        } else if (addr2.find("192.") == 0) {

        } else if (addr2.find("172.") == 0) {
            addr = addr2;
            return addr;
        } else {
            // break;
            // addr = addr2;
        }
    }

    for (int i = 0; i < ips.size(); ++i) {
        addr2 = ips.at(i).to_string();

        if (addr2.find("127.") == 0) {
            // loopback
        } else if (addr2.find("10.") == 0) {
            //
            addr = addr2;
            return addr;
        } else if (addr2.find("192.") == 0) {

        } else if (addr2.find("172.") == 0) {

        } else {
            // break;
            // addr = addr2;
        }
    }

    addr = "127.0.0.1";

    return addr;
}

}  // namespace dht

}  // namespace maidsafe
