// kadserv.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-31 01:10:01 -0700
// Version: $Id$
// 
#ifndef _KADSERV_H_
#define _KADSERV_H_

#include <string>

#include "maidsafe/common/crypto.h"
#include "maidsafe/common/utils.h"
#include "maidsafe/common/securifier.h"

#include "maidsafe/dht/version.h"
#include "maidsafe/dht/contact.h"
#include "maidsafe/dht/return_codes.h"
#include "maidsafe/dht/node_id.h"
#include "maidsafe/dht/node-api.h"
//#include "maidsafe/dht/node_container.h"
#include "maidsafe/dht/contact.h"

#include "nodefunctor.h"
#include "node_container_ex.h"

int kadserv_main(int argc, char **argv);

class KadServer
{
public:
    explicit KadServer();
    virtual ~KadServer();

    bool start();

    static bool WriteBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                            const std::string &filename) ;
    static bool ReadBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                           const std::string &filename);

    // 把任意长度的key格式化为mskad需要的64字节固定长度的值，格式：raw_id_str$000000.....
    // 返回一个格式化字符串的HEX值，长度必为128
    static std::string rewrite_nodeid(const std::string &skey);
    // 把一个key的HEX值还原为格式化字符串，并返回原串raw_id_str，其余的冗余部分被清除掉。
    static std::string restore_nodeid(const std::string &ekey);

protected:
    mk::SecurifierPtr genateSecurifier(const  mk::Contact &contact, const char *rootnode_id, bool newkey);

protected:
    NodeFunctor ncb;
    std::vector<mk::Contact> rncs;
    std::vector<mk::Contact> rncs2;
    // std::vector<mk::NodeContainer<mk::Node> *> nncs;
    std::vector<mk::NodeContainerEx<mk::Node> *> nncs;
    static const int port_base = 8500;
    static const int root_node_count = 8;

    std::function<void(int)> join_fh;
    std::function<void(int)> store_fh;
    std::function<void(int)> delete_fh;
    std::function<void(mk::FindValueReturns)> find_fh;
    std::function<void(int, std::vector<mk::Contact>)> lookup_fh;
};

#endif /* _KADSERV_H_ */
