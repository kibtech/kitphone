// nodeconst.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 18:50:43 +0000
// Version: $Id$
// 

#include <stdio.h>
#include <stdlib.h>
// #include "nodeconst.h"

const char *rootnode_ids[] = {
    "kitech_mskad_dht_rootnode_id_100",
    "kitech_mskad_dht_rootnode_id_101",
    "kitech_mskad_dht_rootnode_id_102",
    "kitech_mskad_dht_rootnode_id_103",
    "kitech_mskad_dht_rootnode_id_104",
    "kitech_mskad_dht_rootnode_id_105",
    "kitech_mskad_dht_rootnode_id_106",
    "kitech_mskad_dht_rootnode_id_107",
    "kitech_mskad_dht_rootnode_id_108",
    NULL
};

const char *rootnode_contacts_file = "./kitech_rootnode_bootstrap_contacts.xml";
