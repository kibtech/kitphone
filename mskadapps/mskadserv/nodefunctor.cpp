// nodefunctor.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-31 01:41:12 -0700
// Version: $Id$
// 

#include "maidsafe/dht/log.h"
#include "maidsafe/dht/node_container.h"

#include "nodefunctor.h"

namespace mk = maidsafe::dht;
namespace mt = maidsafe::transport;

NodeFunctor::NodeFunctor()
{
}

NodeFunctor::~NodeFunctor()
{
}

void NodeFunctor::joinCallback(int result)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<std::endl;
    this->joined(result);
}

void NodeFunctor::storeCallback(int result)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<std::endl;
    this->stored(result);
}

void NodeFunctor::deleteCallback(int result)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<std::endl;
    this->deleted(result);
}


void NodeFunctor::findCallback(maidsafe::dht::FindValueReturns rets)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<rets.return_code<<std::endl;
    if (rets.return_code == mk::kSuccess) {
        mk::ValueAndSignature val;
        std::vector<mk::ValueAndSignature> vals;
        vals = rets.values_and_signatures;
        for (int i = 0; i < vals.size(); ++i) {
            val = vals.at(i);
            std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"fv:"<<val.first<<" siglen:"<<val.second.length()<<std::endl;
        }
    }
    this->valueFound(rets);
}

void NodeFunctor::lookupCallback(int result, std::vector<maidsafe::dht::Contact> contacts)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<" cts:"<< contacts.size()<<std::endl;
    for (int i = 0; i < contacts.size(); ++i) {
        std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"idx:"<<i<<mk::DebugId(contacts.at(i).node_id())<<std::endl;
    }
    this->nodeFound(result, contacts);
}

void NodeFunctor::getContactCallback(int result, maidsafe::dht::Contact  contact)
{
    std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<"result:"<<result<<" cts:"<<mk::DebugId(contact.node_id()) <<std::endl;
    this->contactFound(result, contact);
}
