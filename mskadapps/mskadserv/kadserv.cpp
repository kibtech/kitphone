// kadserv.cpp ---
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-31 01:10:13 -0700
// Version: $Id$
// 
#include <stdlib.h>
#include <stdio.h>

#include <exception>
#include <signal.h>

#include "boost/filesystem.hpp"
#include "boost/program_options.hpp"
#include "boost/archive/xml_iarchive.hpp"
#include "boost/archive/xml_oarchive.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include <boost/thread/condition_variable.hpp>

// #include "maidsafe/common/breakpad.h"

// #include "maidsafe/dht/log.h"
//#include "maidsafe/transport/udp_transport.h"
//#include "maidsafe/transport/tcp_transport.h"
//#include "maidsafe/dht/version.h"
//#include "maidsafe/dht/contact.h"
//#include "maidsafe/dht/return_codes.h"
//#include "maidsafe/dht/node_id.h"
//#include "maidsafe/dht/node-api.h"
//#include "maidsafe/dht/node_container.h"
#include "maidsafe/dht/message_handler.h"
// #include "maidsafe/dht/demo/commands.h"

#include "kadserv.h"
#include "nodefunctor.h"
#include "nodeconst.h"

namespace bptime = boost::posix_time;
namespace fs = boost::filesystem;
namespace bfs3 = boost::filesystem3;
namespace po = boost::program_options;
namespace mk = maidsafe::dht;
namespace mt = maidsafe::transport;


KadServer::KadServer()
{

}

KadServer::~KadServer()
{

}

bool KadServer::WriteBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                                   const std::string &filename) {
    try {
        std::ofstream ofs(filename);
        boost::archive::xml_oarchive oa(ofs);
        boost::serialization::serialize(oa, *bootstrap_contacts, 0);
        // DLOG(INFO) << "Updated bootstrap info.";
    } catch(const std::exception &e) {
        // DLOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    return true;
}

bool KadServer::ReadBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                                  const std::string &filename) {
    try {
        std::ifstream ifs(filename);
        boost::archive::xml_iarchive ia(ifs);
        boost::serialization::serialize(ia, *bootstrap_contacts, 0);
    } catch(const std::exception &e) {
        // DLOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    return true;
}

std::string KadServer::rewrite_nodeid(const std::string &skey)
{
    std::string rkey, rkey2;

    rkey = skey;
    rkey.append("$");
    for (int i = 0; i < mk::kKeySizeBytes - skey.length()-1; i++) {
        rkey.append("0");
    }
    rkey2 = maidsafe::EncodeToHex(rkey);

    std::cout<<"rewrited key length:"<<rkey<<rkey.length()<<rkey2<<std::endl;

    return rkey2;
}

std::string KadServer::restore_nodeid(const std::string &ekey)
{
    std::string skey;
    char dch = '$';

    skey = maidsafe::DecodeFromHex(ekey);
    skey = skey.substr(0, skey.find_first_of(dch));

    std::cout<<"restore key:"<<skey.length()<<skey<<std::endl;
    return skey;
}

mk::SecurifierPtr KadServer::genateSecurifier(const  mk::Contact &contact, const char *rootnode_id, bool newkey)
{
    std::cout<<"gsec:"<<rootnode_id<<newkey<<std::endl;
    mk::SecurifierPtr nsec;
    std::string pubkey, privkey;
    mk::NodeId nid(rewrite_nodeid(std::string(rootnode_id)), mk::NodeId::kHex);

    if (!newkey) {
        std::ifstream nki(std::string("./keys/") + std::string(rootnode_id) + ".pubkey");
        nki >> pubkey;
        std::ifstream nki2(std::string("./keys/") + std::string(rootnode_id) + ".privkey");
        nki2 >> privkey;
        nsec.reset(new maidsafe::Securifier(nid.String(), maidsafe::DecodeFromBase64(pubkey), maidsafe::DecodeFromBase64(privkey)));
    } else {
        maidsafe::crypto::RsaKeyPair key_pair;
        do {
            maidsafe::Sleep(boost::posix_time::milliseconds(50));
            key_pair.GenerateKeys(4096);
            nsec.reset(new maidsafe::Securifier(nid.String(), key_pair.public_key(), key_pair.private_key()));
        } while (key_pair.public_key().empty() || key_pair.private_key().empty());
        std::ofstream nko(std::string("./keys/") + std::string(rootnode_id) + ".pubkey");
        nko << maidsafe::EncodeToBase64(key_pair.public_key());
        std::ofstream nko2(std::string("./keys/") + std::string(rootnode_id) + ".privkey");
        nko2 << maidsafe::EncodeToBase64(key_pair.private_key());
    }

    return nsec;
}

bool KadServer::start()
{
    {
        std::vector<maidsafe::transport::IP> ips = maidsafe::transport::GetLocalAddresses();
        maidsafe::transport::Endpoint endpoint(
            ips.empty() ? maidsafe::transport::IP::from_string("127.0.0.1") : ips.front(), 0);
    }

    // NodeFunctor ncb;
    int iret;
    mk::Contact contact, contact2;
//    std::vector<mk::Contact>  contacts, contacts2;
    mk::NodeContainerEx<mk::Node> *nnc;
    // std::vector<mk::NodeContainer<mk::Node> *> nncs;
    mk::MessageHandlerPtr nmh;
    mk::SecurifierPtr nsec;
    mk::AlternativeStorePtr nstore;
    bool client_only = false;

    join_fh = std::bind(&NodeFunctor::joinCallback, &ncb, std::placeholders::_1);
    store_fh = std::bind(&NodeFunctor::storeCallback, &ncb, std::placeholders::_1);
    delete_fh = std::bind(&NodeFunctor::deleteCallback, &ncb, std::placeholders::_1);
    find_fh = std::bind(&NodeFunctor::findCallback, &ncb, std::placeholders::_1);
    lookup_fh = std::bind(&NodeFunctor::lookupCallback, &ncb, std::placeholders::_1, std::placeholders::_2);

    //int port_base = 8500;
    //int root_node_count = 8;
    std::pair<mt::Port, mt::Port> port_range(port_base, port_base + root_node_count);

//    contacts.clear();
    this->rncs.clear();
    if (boost::filesystem3::exists(boost::filesystem3::path(rootnode_contacts_file))) {
        this->ReadBootstrapFile(&this->rncs, std::string(rootnode_contacts_file));
    }
    for (int i = 0; i < root_node_count; ++i) {
        nnc = new mk::NodeContainerEx<mk::Node>();
        nncs.push_back(nnc);

        nnc->set_join_functor(join_fh);
        nnc->set_store_functor(store_fh);
        nnc->set_delete_functor(delete_fh);
        nnc->set_find_value_functor(find_fh);
        nnc->set_find_nodes_functor(lookup_fh);

        contact = mk::Contact();
        if (rncs.size() > 0) {
            contact = rncs.at(i);
            nsec = this->genateSecurifier(contact, rootnode_ids[i], false);
        } else {
            // 自动按照规则生成一批rootnode,并保存到该文件中
            nsec = this->genateSecurifier(contact, rootnode_ids[i], true);
        }

        nnc->Init(3, nsec, nmh, nstore, client_only);

        // 这个server有问题，在关闭后立即启动会出错。
        iret = nnc->Start(this->rncs2, port_range);
        if (iret == 0) {
            contact2 = nnc->node()->contact();
            rncs2.push_back(contact2);
            std::cout<<i<<" OK "<<iret<<std::endl;
        } else {
            std::cout<<i<<" ERR "<<iret<<std::endl;
        }
    }

    if (rncs.size() == 0) {
        this->WriteBootstrapFile(&rncs2, std::string(rootnode_contacts_file));
    }

    return true;
}

////////////////////////

bool WriteBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                        const std::string &filename) {
  try {
    std::ofstream ofs(filename);
    boost::archive::xml_oarchive oa(ofs);
    boost::serialization::serialize(oa, *bootstrap_contacts, 0);
    // DLOG(INFO) << "Updated bootstrap info.";
  } catch(const std::exception &e) {
    // DLOG(WARNING) << "Exception: " << e.what();
    return false;
  }
  return true;
}

bool ReadBootstrapFile(std::vector<mk::Contact> *bootstrap_contacts,
                       const std::string &filename) {
  try {
    std::ifstream ifs(filename);
    boost::archive::xml_iarchive ia(ifs);
    boost::serialization::serialize(ia, *bootstrap_contacts, 0);
  } catch(const std::exception &e) {
    // DLOG(WARNING) << "Exception: " << e.what();
    return false;
  }
  return true;
}

std::string rewritekey(const std::string &skey)
{
    std::string rkey, rkey2;

    rkey = skey;
    rkey.append("$");
    for (int i = 0; i < mk::kKeySizeBytes - skey.length()-1; i++) {
        rkey.append("0");
    }
    rkey2 = maidsafe::EncodeToHex(rkey);

    std::cout<<"rewrited key length: "<<rkey<<rkey.length()<<rkey2<<std::endl;

    return rkey2;
}

std::string restore_nodeid(const std::string &ekey)  {
    std::string skey;
    char dch = '$';

    skey = maidsafe::DecodeFromHex(ekey);
    skey = skey.substr(0, skey.find_first_of(dch));

    std::cout<<"restore key:"<<skey.length()<<skey<<std::endl;
    return skey;
}

/*
  rootnode, supernode, clientnode
  */
/*
  ./demo f
  */
//int main(int argc, char **argv)
//{
//    bool bret;
//    int iret;
//    boost::mutex nmtx;
//    boost::condition_variable ncondvar;

//    OpResultHandler opr;
//    mk::Contact contact, contact2;
//    std::vector<mk::Contact>  contacts, contacts2;
//    mk::AsioService asvc;
//    uint16_t nk, nalpha, nbeta;
//    boost::posix_time::time_duration ntd;
//    boost::asio::io_service bios;
//    std::pair<mt::Port, mt::Port> port_range(5600, 5604);
//    // maidsafe::transport::UdpTransport *_autp;
//    mk::TransportPtr ntptr(new maidsafe::transport::UdpTransport(asvc));
//    mk::MessageHandlerPtr nmh;
//    mk::SecurifierPtr nsec;
//    mk::AlternativeStorePtr nstore;
//    bool client_only =  argc == 2 ? false : true;
//    mk::Node anode(asvc, ntptr, nmh, nsec, nstore, client_only, nk, nalpha, nbeta, ntd);
//    mk::NodeContainer<mk::Node> nnc;


////    std::function<void(int)> store_fun = std::bind(&OpResultHandler::storeRF, &opr, 123);
////    std::function<void(mk::FindValueReturns)> find_fun =
////            std::bind(&OpResultHandler::FindRF, &opr, mk::FindValueReturns());
//    std::function<void(int)> store_fun = StoreResultor();
//    std::function<void(mk::FindValueReturns)> find_fun = FindVor();
//    std::function<void(int,std::vector<mk::Contact>)> loopup_fun = //LookupVor();
//            std::bind(&OpResultHandler::LoopUpRF, &opr, std::placeholders::_1, std::placeholders::_2);

//    nnc.set_store_functor(store_fun);
//    nnc.set_find_value_functor(find_fun);
//    nnc.set_join_functor(store_fun); // 如果没有的话，会导致程序崩溃
//    nnc.set_find_nodes_functor(loopup_fun); // 如果没有的话，会导致程序崩溃
////    // store_fun(123);
////    nnc.MakeAllCallbackFunctors(&nmtx, &ncondvar);

////    contact = nnc.node()->contact();
////    std::cout<<"node:"<<mk::DebugId(contact.node_id())<<std::endl;

//    nnc.Init(3, nsec, nmh, nstore, client_only);
//    contact = nnc.node()->contact();
//    std::cout<<"node:"<<mk::DebugId(contact.node_id())<<std::endl;


//    if (argc == 2) {
//        // server mode
//        iret = nnc.Start(contacts, port_range);
//        std::cout<<iret<<std::endl;
//    } else {
//        contacts.clear();

//        bret = ::ReadBootstrapFile(&contacts, "abcd.xml");
//        std::cout<<"client boot nodes:"<<contacts.size()<<std::endl;

//        iret = nnc.StartClient(contacts);
//        // iret = nnc.Start(contacts, port_range);
//        std::cout<<"client: "<<iret<<std::endl;


//        mk::NodeId rid("c447d763e98f0313912642ff3f066e9a3bc6e60f110d6c8a21cef43e3678f62636f9c7f5a7be2e238cf1a65f50ea8329a1a32b66853248220b655dbf865465f7");
//        // nnc.Join(rid, contacts);

//    }
//    // anode.Join();

//    // contacts.push_back(contact);
//    // contacts.push_back(contact2);

//    contact = nnc.node()->contact();
//    std::cout<<"node:"<<mk::DebugId(contact.node_id())<<std::endl;

//    contacts2 = nnc.bootstrap_contacts();
//    contacts2.push_back(contact);
//    if (argc == 2) {
//        // contacts2.push_back(contact);
//        bret = ::WriteBootstrapFile(&contacts2, "abcd.xml");
//    }

//    // bret = ::ReadBootstrapFile(&contacts2, "abcd.xml");
//    std::cout<<contacts.size()<< contacts2.size()<<std::endl;

//    std::string stval = contact.node_id().ToStringEncoded(mk::NodeId::kBase64);
//    std::string stkey, fstkey;
//    fstkey = "stid_liuguangzhao987654321";
//    if (argc == 2) {
//        stkey = "stid_liuguangzhao987654321";
//    } else {
//        stkey = "stid_drswinghead1233dsfdsf";
//    }
//    stkey = rewritekey(stkey);
//    fstkey = rewritekey(fstkey);

//    mk::Key skey(stkey, (mk::NodeId::EncodingType)mk::NodeId::kHex);
//    mk::Key fskey(fstkey, (mk::NodeId::EncodingType)mk::NodeId::kHex);

//    std::cout<<"Storing .."<<stkey<<" -> "<<stval<<skey.IsValid()<<fskey.IsValid()<<std::endl;


//    nnc.Store(skey, stval, std::string(),
//              boost::posix_time::time_duration(1, 0, 0, 0), mk::SecurifierPtr());

//    nnc.FindValue(fskey, mk::SecurifierPtr());

//    nnc.FindNodes(fskey);


////    while (1) {
////        // boost::unique_lock<boost::mutex> lock(nmtx);
////        boost::mutex::scoped_lock lock(nmtx);
////        ncondvar.wait(lock);
////        // DLOG(INFO)<<"aaaaaaaaaa";
////        std::cout<<__FILE__<<__LINE__<<__FUNCTION__<<std::endl;
////    }

//    std::cout<<"sleeping... for svc running severval seconds."<<std::endl;
//    boost::this_thread::sleep(boost::posix_time::millisec(50000));
//    boost::this_thread::sleep(boost::posix_time::seconds(3));
//    boost::this_thread::sleep(boost::posix_time::seconds(3000));
//    return 0;
//}



//int kadserv_main(int argc, char **argv)
//{
//    NodeFunctor ncb;
//    int iret;
//    mk::Contact contact, contact2;
//    std::vector<mk::Contact>  contacts, contacts2;
//    mk::NodeContainer<mk::Node> *nnc;
//    std::vector<mk::NodeContainer<mk::Node> *> nncs;
//    mk::MessageHandlerPtr nmh;
//    mk::SecurifierPtr nsec;
//    mk::AlternativeStorePtr nstore;
//    bool client_only = false;
//    const char *bs_contacts = "./kitech_rootnode_bootstrap_contacts.xml";
//    std::function<void(int)> join_fh = std::bind(&NodeFunctor::joinCallback, &ncb, std::placeholders::_1);
//    std::function<void(int)> store_fh = std::bind(&NodeFunctor::storeCallback, &ncb, std::placeholders::_1);
//    std::function<void(mk::FindValueReturns)> find_fh =
//            std::bind(&NodeFunctor::findCallback, &ncb, std::placeholders::_1);
//    std::function<void(int, std::vector<mk::Contact>)> lookup_fh =
//            std::bind(&NodeFunctor::lookupCallback, &ncb, std::placeholders::_1, std::placeholders::_2);

//    int port_base = 8500;
//    int root_node_count = 8;
//    int port_n;
//    std::pair<mt::Port, mt::Port> port_range(port_base, port_base + root_node_count);

//    if (!boost::filesystem3::exists(boost::filesystem3::path(bs_contacts))) {
//        // 自动按照规则生成一批rootnode,并保存到该文件中
//        contacts.clear();
//        for (int i = 0; i < root_node_count; ++i) {
//            nnc = new mk::NodeContainer<mk::Node>();
//            nncs.push_back(nnc);

//            nnc->set_join_functor(join_fh);
//            nnc->set_store_functor(store_fh);
//            nnc->set_find_value_functor(find_fh);
//            nnc->set_find_nodes_functor(lookup_fh);

//            {
//                mk::NodeId nid(rewritekey(rootnode_ids[i]), mk::NodeId::kHex);
//                maidsafe::crypto::RsaKeyPair key_pair;
//                do {
//                    maidsafe::Sleep(boost::posix_time::milliseconds(10));
//                    key_pair.GenerateKeys(4096);
//                    //                  std::string id(mk::NodeId(NodeId::kRandomId).String());
//                    nsec.reset(new maidsafe::Securifier(nid.String(),
//                                                        key_pair.public_key(),
//                                                        key_pair.private_key()));
//                } while (key_pair.public_key().empty() || key_pair.private_key().empty());
//                std::ofstream nko(std::string("./keys/") + std::string(rootnode_ids[i]) + ".pubkey");
//                nko << maidsafe::EncodeToBase64(key_pair.public_key());
//                std::ofstream nko2(std::string("./keys/") + std::string(rootnode_ids[i]) + ".privkey");
//                nko2 << maidsafe::EncodeToBase64(key_pair.private_key());
//            }

//            nnc->Init(3, nsec, nmh, nstore, client_only);

//            iret = nnc->Start(contacts, port_range);
//            if (iret == 0) {
//                contact = nnc->node()->contact();
//                contacts.push_back(contact);
//                std::cout<<i<<" OK "<<iret<<std::endl;
//            } else {
//                std::cout<<i<<" ERR "<<iret<<std::endl;
//            }
//        }
//        WriteBootstrapFile(&contacts, std::string(bs_contacts));
//    } else {
//        // 从文件中读取rootnode，使用这些rootnode的相关参数，启动相应的rootnode
//        contacts2.clear();
//        ReadBootstrapFile(&contacts2, std::string(bs_contacts));

//        std::string pubkey, privkey;
//        std::string nsid,tmp;
//        for (int i = 0; i < contacts2.size(); ++i) {
//            contact2 = contacts2.at(i);
//            nsid =contact2.node_id().ToStringEncoded(mk::NodeId::kHex);
//            tmp = restore_nodeid(nsid);
//            std::cout<<"Loading:"<<i<<contact2.node_id().IsValid()<<tmp<<tmp.length()<<std::endl;

//            std::ifstream nki(std::string("./keys/") + std::string(rootnode_ids[i]) + ".pubkey");
//            nki >> pubkey;
//            std::ifstream nki2(std::string("./keys/") + std::string(rootnode_ids[i]) + ".privkey");
//            nki2 >> privkey;

//            nnc = new mk::NodeContainer<mk::Node>();
//            nncs.push_back(nnc);

//            nnc->set_join_functor(join_fh);
//            nnc->set_store_functor(store_fh);
//            nnc->set_find_value_functor(find_fh);
//            nnc->set_find_nodes_functor(lookup_fh);

//            mk::NodeId nid(rewritekey(tmp), mk::NodeId::kHex);
//            nsec.reset(new maidsafe::Securifier(nid.String(), maidsafe::DecodeFromBase64(pubkey), maidsafe::DecodeFromBase64(privkey)));

//            nnc->Init(3, nsec, nmh, nstore, client_only);

//            iret = nnc->Start(contacts, port_range);
//            if (iret == 0) {
//                contact = nnc->node()->contact();
//                contacts.push_back(contact);
//                std::cout<<i<<" OK "<<iret<<std::endl;
//            } else {
//                std::cout<<i<<" ERR "<<iret<<std::endl;
//            }
//        }

//        //        WriteBootstrapFile(&contacts, std::string(bs_contacts));
//    }

//    pause();
//    return 0;
//}
