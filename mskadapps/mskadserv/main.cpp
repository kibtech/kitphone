// main.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-31 01:09:53 -0700
// Version: $Id$
// 

#include <stdlib.h>
#include <stdio.h>

#include "kadserv.h"

int main(int argc, char **argv)
{
    // return kadserv_main(argc, argv);

    KadServer ks;

    ks.start();

    std::cout<<"Started."<<std::endl;
    pause();
    return 0;
}
