// nodeconst.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-01 18:50:26 +0000
// Version: $Id$
// 

#ifndef _NODECONST_H_
#define _NODECONST_H_

extern const char *rootnode_ids[];
extern const char *rootnode_contacts_file;

#endif /* _NODECONST_H_ */
