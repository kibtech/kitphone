// nodefunctor.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-31 01:40:56 -0700
// Version: $Id$
// 
#ifndef _NODEFUNCTOR_H_
#define _NODEFUNCTOR_H_

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/signals2.hpp>

// #include "maidsafe/dht/log.h"
#include "maidsafe/dht/node-api.h"
#include "maidsafe/dht/contact.h"

class NodeFunctor
{
public:
    NodeFunctor();
    virtual ~NodeFunctor();

    void joinCallback(int result);
    void storeCallback(int result);
    void deleteCallback(int result);
    void findCallback(maidsafe::dht::FindValueReturns rets); // for find value
    void lookupCallback(int result, std::vector<maidsafe::dht::Contact> contacts); // lookup nodes
    void getContactCallback(int result, maidsafe::dht::Contact  contact); //

    boost::signals2::signal<void(int)> joined;
    boost::signals2::signal<void(int)> stored;
    boost::signals2::signal<void(int)> deleted;
    boost::signals2::signal<void(maidsafe::dht::FindValueReturns)> valueFound;
    boost::signals2::signal<void(int, std::vector<maidsafe::dht::Contact>)> nodeFound;
    boost::signals2::signal<void(int, maidsafe::dht::Contact)> contactFound;

};



#endif /* _NODEFUNCTOR_H_ */
