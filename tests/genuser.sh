#!/bin/sh

USER_CNT=50
SIP_USER_FILE=/tmp/sip_user_load.conf
AST_DIALPLAN_FILE=/tmp/ast_user_load.conf
USER_PREFIX=pmeijin

echo "" > $SIP_USER_FILE
echo "" > $AST_DIALPLAN_FILE

idx=0
while true
do
    if [ $idx -ge $USER_CNT ] ; then
        break;
    fi

    USER_SUFFIX=`expr $idx + 10`
    USER_NAME=${USER_PREFIX}${USER_SUFFIX}
    echo "["${USER_NAME}"]" >> $SIP_USER_FILE
    echo "type=friend" >> $SIP_USER_FILE
    echo "username=$USER_NAME" >> $SIP_USER_FILE
    echo "secret=1415926" >> $SIP_USER_FILE
    echo "host=dynamic" >> $SIP_USER_FILE
    echo "nat=yes" >> $SIP_USER_FILE
    echo "allow=ulaw" >> $SIP_USER_FILE
    echo "allow=alaw" >> $SIP_USER_FILE
    echo "" >> $SIP_USER_FILE

    ##########
    DP_MID=`expr 5060 + $idx`
    echo "exten => _9900866${DP_MID}.,1,NoOp(CALLERID(num)|\${CALLERID(num)}|)" >> $AST_DIALPLAN_FILE
    echo "exten => _9900866${DP_MID}.,n,Dial(SIP/${USER_NAME},1000,g)" >> $AST_DIALPLAN_FILE
    echo "exten => _9900866${DP_MID}.,n,Hangup()" >> $AST_DIALPLAN_FILE
    echo "" >> $AST_DIALPLAN_FILE

    idx=`expr $idx + 1`
done
