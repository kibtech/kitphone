#!/bin/sh
set -x

#######readme
# args: client/server

#####configure
SKYPEKIT=$HOME/skypekit/distro-skypekit-cb3.1_3.1.0.2665_124773/bin/linux-x86/linux-x86-skypekit-novideo
SKYPEKIT_PORT_BASE=8970
SIP_PORT_BASE=7000
SIPBOT=/serv/bin/pjsuad
if [ ! -f $SIPBOT ] ; then
    SIPBOT=$HOME/xfsdev/pjproject-1.8.5/pjsip-apps/bin/pjsua-i686-pc-linux-gnu
fi
PLAY_FILE=$HOME/ivr_record/SKYPE1.wav
if [ ! -f $PLAY_FILE ] ; then
    PLAY_FILE=$HOME/SKYPE1.wav
fi
SIPBOT_STATIC_ARGS="--realm=* --null-audio --auto-answer=200 --auto-play --play-file=$PLAY_FILE"
SIPUSER_PREFIX=pmeijin
BOT_CNT=20
VOMCALL=$HOME/kitphone-svn/tests/vomcall
if [ ! -f $VOMCALL ] ; then
    VOMCALL=$HOME/xfsdev/kitphone-svn/tests/vomcall
fi
LOGIN_USER_NAMES=(pmeijin0060 pmeijin0061 pmeijin0062 pmeijin0063 pmeijin0064 pmeijin0065 pmeijin0066 pmeijin0067 pmeijin0068 pmeijin0069 pmeijin0070 pmeijin0071 pmeijin0072 pmeijin0073 pmeijin0074 pmeijin0075 pmeijin0076 pmeijin0077 pmeijin0078 pmeijin0079 pmeijin0080)
LOGIN_PASSWORDS=(tomonline796521M tomonline118607M tomonline74358M tomonline930590M tomonline550346M tomonline767629M tomonline67925M tomonline192942M tomonline510201M tomonline539018M tomonline901406M tomonline10696M tomonline157229M tomonline480049M tomonline714269M tomonline795891M tomonline736676M tomonline398733M tomonline594005M tomonline984976M tomonline198141M)

########

if [ $# = "1" ] ; then
    true;
else
    echo "ERR: prog <client|server>";
fi

idx=0;
while true
do
    if [ "$1" = "client" ] ; then
        true;
    else
        break;
    fi
    if [ $idx -ge $BOT_CNT ] ; then
        break;
    fi
    ###########

    SIPUSER_SUFFIX=`expr $idx + 10`
    SIPUSER="${SIPUSER_PREFIX}${SIPUSER_SUFFIX}"
    SKYPEKIT_PORT=`expr $idx + $SKYPEKIT_PORT_BASE`

    SKYPEKIT_PORT=`expr $idx + $SKYPEKIT_PORT_BASE`
    $SKYPEKIT -m -p $SKYPEKIT_PORT > /tmp/skypekit_$idx.log 2>&1 &
    SKYPEKIT_PID=$!

    LOGIN_USER_NAME=${LOGIN_USER_NAMES[$idx]}
    LOGIN_PASSWORD=${LOGIN_PASSWORDS[$idx]}
    DP_MID=`expr 5060 + $idx`
    PHONE_NUMBER=9900866${DP_MID}013552776960
    $VOMCALL -p $SKYPEKIT_PORT -u $LOGIN_USER_NAME -w $LOGIN_PASSWORD -n $PHONE_NUMBER -r liuguangzhao      > /tmp/vomcall_$idx.log 2>&1 &
    VOMCALL_PID=$!
    
    ##########
    idx=`expr $idx + 1`;    
done

#########server
idx=0;
while true
do
    if [ "$1" = "server" ] ; then
        true;
    else
        break;
    fi
    if [ $idx -ge $BOT_CNT ] ; then
        break;
    fi
    ###########

    SIPUSER_SUFFIX=`expr $idx + 10`
    SIPUSER="${SIPUSER_PREFIX}${SIPUSER_SUFFIX}"
    SIPPASS=1415926
    SIPPORT=`expr $idx + $SIP_PORT_BASE`

    SIPBOT_DYNAMIC_ARGS="--local-port=$SIPPORT --id=sip:$SIPUSER@sip.qtchina.net:$SIPPORT --registrar=sip:$SIPUSER@202.108.29.234:4060 --username=$SIPUSER --password=$SIPPASS --next-account "

    $SIPBOT $SIPBOT_STATIC_ARGS $SIPBOT_DYNAMIC_ARGS >/tmp/sipbot_$idx.log 2>&1 &
    SIPBOT_PID=$!
    
    ##########
    idx=`expr $idx + 1`;    
done

echo "Done.";