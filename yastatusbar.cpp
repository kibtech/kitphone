// yastatusbar.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-16 02:05:50 +0000
// Version: $Id$
// 

#include <QLayout>

#include "simplelog.h"

#include "yastatusbar.h"

YaStatusBar::YaStatusBar(QWidget *parent)
    :QStatusBar(parent)
{
    this->lbar = new QStatusBar();
    this->lbar->setSizeGripEnabled(false);
    this->rbar = new QStatusBar();
    this->rbar->setSizeGripEnabled(true);
    QObject::connect(this->rbar, SIGNAL(messageChanged(QString)), this, SIGNAL(messageChanged(QString)));

    QHBoxLayout *l = new QHBoxLayout();
    l->addWidget(this->lbar);
    l->addWidget(this->rbar);
    this->setLayout(l);

    qLogx()<<this->lbar->rect()<<this->lbar->size();
    qLogx()<<this->rbar->rect()<<this->rbar->size();
}

YaStatusBar::~YaStatusBar()
{

}

void YaStatusBar::addWidget(QWidget *widget, int stretch)
{
    this->lbar->addWidget(widget, stretch);
    QSize s = this->rbar->size();
    s.setWidth(s.width()-100);
    this->rbar->setBaseSize(s);
}

int YaStatusBar::insertWidget(int index, QWidget *widget, int stretch)
{

    QSize s = this->rbar->size();
    s.setWidth(s.width()-100);
    this->rbar->setBaseSize(s);

    return     this->lbar->insertWidget(index, widget, stretch);
}

void YaStatusBar::addPermanentWidget(QWidget *widget, int stretch)
{
    this->rbar->addPermanentWidget(widget, stretch);
}

int YaStatusBar::insertPermanentWidget(int index, QWidget *widget, int stretch)
{
    return this->rbar->insertPermanentWidget(index, widget, stretch);
}

void YaStatusBar::removeWidget(QWidget *widget)
{
    this->lbar->removeWidget(widget);
}

//    void setSizeGripEnabled(bool);
//    bool isSizeGripEnabled() const;

QString YaStatusBar::currentMessage() const
{
    return this->rbar->currentMessage();
}

void YaStatusBar::showMessage(const QString &text, int timeout)
{
    this->rbar->showMessage(text, timeout);
}

void YaStatusBar::clearMessage()
{
    this->rbar->clearMessage();
}
