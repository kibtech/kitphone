<?php
/* skype_gateway.php --- 
 * 
 * Author: liuguangzhao
 * Copyright (C) 2007-2010 liuguangzhao@users.sf.net
 * URL: 
 * Created: 2010-11-11 14:46:34 +0800
 * Version: $Id$
 */

$cfg_file = realpath("../../../../xfsdev/kitphone-svn/skyserv/.skyserv.ini");
if (!file_exists($cfg_file)) {
    echo "404 File Not Exists\n";
    exit();
}


$host = "";
$port = "";
$user = "";
$passwd = "";

$host = '172.24.149.193';
$username = 'postgres';
$password = 'post.DB#2872';
$dbname   = 'karia2_resource';
// $dbname   = 'voice_gateways_2';

$conn_info = "host=$host port=5432 dbname=$dbname user=$username password=$password";

$dbconn = pg_connect($conn_info);
if (!$dbconn) {
    echo "501 Connect database error\n";
    exit();
}

function my_escape_string($str)
{
    $estr = pg_escape_string($str);
    return $estr;    
}

if ($_GET['func'] == 'acquire') {
    $caller_name = my_escape_string($_GET['caller_name']);
    $callee_phone = my_escape_string($_GET['callee_phone']);
    $area_type = my_escape_string($_GET['area_type']); // TODOing

    if ($caller_name == '' || $callee_phone == '') {
        echo "102 Client invalid params\n";
        exit();
    }
    
    $sql = "UPDATE skype_gateways SET in_use = 1, lock_time=NOW(),caller_name='$caller_name',callee_phone='$callee_phone' WHERE in_use = 0 AND skype_id = (SELECT skype_id FROM skype_gateways WHERE in_use=0 ORDER BY RANDOM() LIMIT 1)";
    // echo $sql;
    $res = pg_query($dbconn, $sql);
    if (!$res) {
        echo "502 Query database error\n";
        exit();
    }
    if (pg_affected_rows($res) != 1) {
        echo "404 Not found\n";
        exit();
    }

    $sql2 = "SELECT skype_id FROM skype_gateways WHERE in_use=1 AND caller_name='$caller_name'";
    $res = pg_query($dbconn, $sql2);
    $rows = pg_fetch_all($res);
    // print_r($rows);
    if (count($rows) == 0) {
        echo "404 Not found\n";
        exit();
    }        

    echo "200 OK\n";
    echo $rows[0]['skype_id'] . "\n";
    exit();
} else if ($_GET['func'] == 'release') {
    $caller_name = my_escape_string($_GET['caller_name']);
    $gateway = my_escape_string($_GET['gateway']);

    if ($caller_name == '' || $gateway == '') {
        echo "102 Client invalid params\n";
        exit();
    }

    $sql = "UPDATE skype_gateways SET in_use = 0, lock_time=NOW() WHERE skype_id='$gateway' AND in_use=1";
    $res = pg_query($dbconn, $sql);
    if (!$res) {
        echo "503 Update database faild\n";
        exit();
    }
    if (pg_affected_rows($res) != 1) {
        echo "404 Not found\n";
        exit();
    }
    echo "200 OK\n";
} else if ($_GET['func'] == 'notepair') {
    // ?func=funcname&caller_name=skype_id&callee_phone=pstn_number&rand=randint
    $caller_name = my_escape_string($_GET['caller_name']);
    $callee_phone = my_escape_string($_GET['callee_phone']);
    $caller_info = my_escape_string($_GET['hinfo']);
    $caller_ipaddr = $_SERVER['REMOTE_ADDR'];
    
    if ($caller_name == '' || $callee_phone == '' 
       || strlen($callee_phone) <= 7 || strlen($callee_phone) > 26) {
        echo "102 Client invalid params\n";
        exit();
    }

    if (substr($callee_phone, 0, 1) == "*") {
        $callee_phone = "99008668056" . substr($callee_phone, 1);
    } else if (substr($callee_phone, 0, 7) == '9900866') {

    } else {
      	// wrong phone_number;
    }

    $sql = "SELECT merge_replace_skype_call_pairs('{$caller_name}', '{$callee_phone}', '{$caller_ipaddr}')";
    $res = pg_query($dbconn, $sql);
    if (!$res) {
        echo "503 Update database faild.\n";
        exit();
    }

    $sql = "SELECT skype_id,in_service FROM skype_routers WHERE in_service=1 ORDER BY random() LIMIT 3";
    $res = pg_query($dbconn, $sql);
    if (!$res) {
        echo "504 Select database faild.\n";
        exit();
    }
    $row_cnt = pg_num_rows($res);
    $rows = pg_fetch_all($res);
    // print_r($rows);

    echo "200 OK\n";
    echo "$row_cnt\n";
    for ($i = 0 ; $i < $row_cnt; $i ++) {
        $skype_id = $rows[$i]['skype_id'];
        $in_service = $rows[$i]['in_service'];
        echo "$skype_id\n";
    }
} else if ($_GET['func'] == 'getpair') {
    $caller_name = my_escape_string($_GET['caller_name']);
    $callee_phone = my_escape_string($_GET['callee_phone']);
    $caller_ipaddr = $_SERVER['REMOTE_ADDR'];

    if ($caller_name == '' || $callee_phone == '') {
        echo "102 Client invalid params\n";
        exit();
    }

    $sql = "SELECT callee_phone, lock_time FROM skype_callpairs WHERE skype_id='{$caller_name}' AND EXTRACT(EPOCH from NOW()-lock_time) < 3600.0 ORDER BY lock_time DESC LIMIT 1";

    $res = pg_query($dbconn, $sql);
    if (!$res) {
        echo "503 Update database faild\n";
        exit();
    }
     
    $row_cnt = pg_num_rows($res);
    $row = pg_fetch_row($res);

    echo "200 OK\n";
    echo "{$row_cnt}\n";
    echo "{$caller_name},{$row[0]},{$row[1]}\n";
} else {
    echo "100 Client not supported\n";
    exit();
}

exit();

?>
