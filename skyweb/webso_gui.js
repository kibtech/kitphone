// 
// need jquery 1.5.x
// need jquery-ui 1.8.10

$(function() {
	// init gui elements
	$("#dlg_com_not_installed").dialog({
		autoOpen: false,
		    modal: true,
		    width: 370,
		    height: 220,
		    buttons: [
			      {
				  text: "我想多了解一下，暂时不安装",
				  click: on_donot_install_plugin
			      },
			      {
				  text: "马上安装",
				  click: on_install_plugin_now
			      }
		    ],
		    open: function() { 
		              $(this).parents('.ui-dialog-buttonpane button:eq(1)').focus();
		          }
	  });

	$("#dlg_com_installation").dialog({
		autoOpen: false,
		    modal: true,
		    width: 430,
		    height: 280
// 		    buttons: [
// 			      {
// 				  text: "确定",
// 				  click: function() {
// 				      // alert('安装完成');
// 				      $(this).dialog("close");
// 				  }
// 			      },
// 		    ],
// 		    open: function() { 
// 		              $(this).parents('.ui-dialog-buttonpane button:eq(1)').focus();
// 		          }
	  });

	$("#win_dialpanel").dialog({
		autoOpen: false,
		    model: false,
		    resizable: false,
		    width: 233,
		    height: 195
	});

	for (var i = 0; i <= 9; i++) {
	    $("#digit_"+i).button({
		    label: "&nbsp;&nbsp;"+i+"&nbsp;&nbsp;"
		});
	    $("#digit_"+i).click(function() {
		    // alert(this);
		    var digit_v = ($(this).attr("innerText")).substr(2,1);
		    on_digit_button_clicked(digit_v, this);
		});
	}
	$("#digit_j").button({
		label: "&nbsp;&nbsp;#&nbsp;&nbsp;"
	    });
	$("#digit_j").click(function() {
		// alert(this);
		on_digit_button_clicked("#", this);
	    });
	$("#digit_a").button({
		label: "&nbsp;&nbsp;*&nbsp;&nbsp;"
	    });
	$("#digit_a").click(function() {
		// alert(this);
		on_digit_button_clicked("*", this);
	    });

	//
	function reposition_dialpanel() {
	    var pos_base = $("#phone_number");
	    // alert(obj_desktop.offset().top);
	    var aimwin = $("#win_dialpanel");
	    // alert(obj_desktop.height());  /// ok
	    // alert(setup_1.height());  /// ok
	    // dlg.top =  (pa.height - dlg.height)/2 + pa.top
	    // dlg.left = (pa.width - dlg.width)/2 + pa.left
	    var dialpanel_pos = [
			       pos_base.offset().left + 100
			       ,
			       pos_base.offset().top + 50
			       ];
	    if ($.browser.msie) {
		dialpanel_pos = [
			     pos_base.offset().left + (pos_base.width() - 215)/2,
			     pos_base.offset().top - 202
			   ];
	    } else {
		// for opera,firefox,chromium
		aimwin.dialog("option", "width", 236);
		aimwin.dialog("option", "height", 195);
		for (var i = 1; i <= 3; i++) {
		    $("#dialp_sep"+i).attr("innerHTML", "");
		    $("#dialp_sep"+i).css("height", 5);
		}
		dialpanel_pos = [
			     pos_base.offset().left + (pos_base.width() - 215)/2,
			     pos_base.offset().top - 202
			   ];		
	    }
	    // alert(setup_1_pos);
	    // setup_1.dialog("option", "position", [300,500]);
	    // setup_1.dialog("option", "position", setup_1_pos);
	    // setup_1.dialog("open");
	    aimwin.dialog("option", "position", dialpanel_pos);
	    // aimwin.dialog("open");
	}
	reposition_dialpanel();

	// init clear history dialog
	$("#dlg_clear_dial_history").dialog({
		autoOpen: false,
		    modal: true,
		    width: 380,
		    height: 200,
		    buttons: [
			      {
				  text: "确定",
				  click: function() {
				      // alert('安装完成');
				      $(this).dialog("close");
				  }
			      },
		    ]
// 		    open: function() { 
// 		              $(this).parents('.ui-dialog-buttonpane button:eq(1)').focus();
// 		          }
	  });

	$("#pbar_clear_dial_history").progressbar({
		value: 0
        });
    });
