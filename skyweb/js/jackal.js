// JavaScript Document
(function () {
    if (!window.ADS) {
        window['ADS'] = {};
    }

    function isCompatible(other) {
        //使用能力检测来检查必要条件
        if (other === false || !Array.prototype.push || !Object.hasOwnProperty || !document.createElement || !document.getElementsByTagName) {
            return false;
        }
        return true;
    }
    window['ADS']['isCompatible'] = isCompatible;
    /*可获取多个ID..形成数组..*/
    function $() {
        //声明返回数组..
        var elements = new Array();
        //不在函数内部声明参数的时候用arguments直接检测...这样的..

        for (var i = 0; i < arguments.length; i++) {
            var element = arguments[i];
            //如果该参数是一个字符串假设它是一个id。
            if (arguments.length == 1) {

                if (typeof element == 'string') {
                    element = document.getElementById(element);

                }
                return element;

            }

            elements.push(element);

        }
        //返回包含多个被请求元素的数组..
        return elements;

    };
    window['ADS']['$'] = $;



    /**
    * Register an event listener on an element
    */
    function addEvent(node, type, listener) {
        // Check compatibility using the earlier method
        // to ensure graceful degradation
        if (!isCompatible()) { return false }
        if (!(node = $(node))) return false;

        if (node.addEventListener) {
            // W3C method
            node.addEventListener(type, listener, false);
            //alert("W3C")
            return true;
        } else if (node.attachEvent) {
            // MSIE method
            node['e' + type + listener] = listener;
            node[type + listener] = function () { node['e' + type + listener](window.event); }
            node.attachEvent('on' + type, node[type + listener]);
            //alert("IE")
            return true;
        }

        // Didn't have either so return false
        return false;
    };
    window['ADS']['addEvent'] = addEvent;


    function removeEvent(node, type, listener) {

        if (!(node = $(node))) { return false; }

        if (node.removeEventListener) {
            node.removeEventListener(type, listener, false);
            return true;
        }
        else if (node.detachEvent) {
            node.detachEvent('on' + type, node[type + listener]);
            node[type + listener] = null;
            return true;
        }
        return false;




    };
    window['ADS'][removeEvent] = removeEvent;

    function getElementsByClassName(className, tag, parent) {
        parent = parent || document;
        if (!(parent = $(parent))) return false;

        // Locate all the matching tags
        var allTags = (tag == "*" && parent.all) ? parent.all : parent.getElementsByTagName(tag);
        var matchingElements = new Array();

        // Create a regular expression to determine if the className is correct
        className = className.replace(/\-/g, "\\-");
        var regex = new RegExp("(^|\\s)" + className + "(\\s|$)");

        var element;
        // Check each element
        for (var i = 0; i < allTags.length; i++) {
            element = allTags[i];
            if (regex.test(element.className)) {
                matchingElements.push(element);
            }
        }

        // Return any matching elements
        return matchingElements;
    };
    window['ADS']['getElementsByClassName'] = getElementsByClassName;

    /*焦点图.*/
    function toggleDisplay(node, value) {
        if (!(node = $(node))) { return false; }
        if (node.style.display != 'none') {
            node.style.display = 'none'
        }
        else {
            node.style.display = value || '';
        }
        return true;

    }
    window['ADS']['toggleDisplay'] = toggleDisplay;

    /* 
    切换图..
    ADS.addEvent(window,'load',function(W3CEvent){
    
    ADS.addEvent(ADS.$("b"),'click',function(){
        
    if(ADS.$('a').style.visibility=='hidden')
    {
    ADS.$('a').style.visibility='visible';
    }
    else{
    ADS.$('a').style.visibility='hidden';
    }
    
    })
    })

    */
    /*
    下拉框切换..兼容IE 6..
    ADS.addEvent(window,'load',function(){
    
    var elements=ADS.getElementsByClassName("menu",'li',ADS.$("navigation"))
    
    for(var i=0;i<elements.length;i++){
    
    ADS.addEvent(elements[i],'mouseover',function(){
    this.getElementsByTagName("ul")[0].style.display='block'     
            
    });
        
    ADS.addEvent(elements[i],'mouseout',function(){
            
    this.getElementsByTagName("ul")[0].style.display='none'
                
            
    });
          
    }

    });

    */

    function insertAfter(node, referenceNode) {
        if (!(node = $(node))) return false;
        if (!(referenceNode = $(referenceNode))) return false;
        return referenceNode.parentNode.insertBefore(node, referenceNode.next)
        /*注意这个操作文档的几个方法..*/
    }

    window['ADS']['insertAfter'] = insertAfter;

    function removeChildren(parent) {
        if (!(parent = $(parent))) { return false; }
        while (parent.firstChild) {
            parent.firstChild.parentNode.removeChild(parent.firstChild);
        }

        return parent;

    };
    window['ADS']['removeChildren'] = removeChildren;


    //XML文档空格bug
    function clearWhitespace(element) {
        element = element || document;
        var cur = element.firstChild;

        while (cur != null) {
            if (cur.nodeType == 3 && !/\S/.test(cur.nodeValue)) {
                element.removeChild(cur);
            }
            else if (cur.nodeType == 1) {
                clearWhitespace(cur); //深层次递归..
            }
            cur = cur.nextSibling;
        }

    }
    window['ADS']['clearWhitespace'] = clearWhitespace;

    //查找下一个兄弟节点
    function next(elem) {
        do {
            elem = elem.nextSibling;
        }
        while (elem && elem.nodeType != 1)
        return elem;
    }
    window['ADS']['next'] = next;
    //查找上一个兄弟节点
    function prev(elem) {
        do {
            elem = elem.previousSibling;
        } while (elem && elem.nodeType != 1);
        return elem;
    }
    window["ADS"]['prev'] = prev;
    //查找元素第一个子元素的函数.
    function first(elem) {
        elem = elem.firstChild;
        if (elem && elem.nodeType != 1) {
            next(elem);
        }
        else {
            return elem;
        }
        //return elem && elem.nodeType != 1 ? first(elem) : elem;
    }
    window["ADS"]['first'] = first;
    //最后一个元素..
    function last(elem) {
        elem = elem.lastChild;
        if (elem && elem.nodeType != 1) {
            prev(elem);
        }
        else {
            return elem;
        }
    }
    window["ADS"]['last'] = last;

    function parent(elem, num) {
        num = num || 1;
        for (var i = 0; i < num; i++) {
            if (elem != null) elem = elem.parentNode;
            return elem;
        }
    }
    window["ADS"]['parent'] = parent;

    function tag(name, elem) {
        return (elem || document).getElementsByTagName(name);
    }
    window["ADS"]['tag'] = tag;

    function id(name) {
        return document.getElementById(name);
    }
    window["ADS"]['id'] = id;

    /*  
    domready函数推荐使用$(document).ready
    简写至$(function(){})
    function domReady(f) {
    if (domReady.done) return f();
    if (domReady.timer) {
    domReady.ready.push(f);
    } else {
    addEvent(window, 'load', isDOMReady);
    domReady.ready = [f];
    domReady.timer = setInterval(isDOMReady, 13);
    }
    }
    window["ADS"]["domReady"]=domReady;
    function isDOMReady() {
    if (domReady.done) return false;
    if (document && document.getElementsByTagName && document.getElementById && document.body) {
    clearInterval(domReady.timer);
    domReady.timer = null;
    for (var i=0; i<domReady.ready.length; i++) {
    domReady.ready[i]();
    }
    domReady.ready = null;
    domReady.done = true;
    }
    }
    */

    function hasClass(name, type) {
        var r = [];
        var re = new RegExp("(^|\\s)" + name + "(\\s|$)");
        var e = document.getElementsByTagName(type || "*");
        for (var j = 0; j < e.length; j++) {
            if (re.test(e[j])) r.push(e[j]);

            return r;
        }
    }
    window["ADS"]["hasClass"] = hasClass;

    /*<h1>Hello <span>World</span></h1>*/
    function text(e) {
        var t = "";
        e = e.childNodes || e;
        for (var j = 0; j < e.length; j++) {
            t += e[j].nodeType != 1 ? e[j].nodeValue : text[e[j].childNodes];
        }
        return t;
    }
    window["ADS"]["text"] = text;
    function hasAttribute(elem, name) {
        return elem.getAttribute(name) != null;
    }
    window["ADS"]["hasAttribute"] = hasAttribute;

    function attr(elem, name, value) {
        if (!name || name.constructor != String) return '';
        name = { 'for': 'htmlFor', 'class': 'className'}[name] || name;
        if (typeof value != 'undefined') {
            elem[name] = value;

            if (elem.setAttribute)
                elem.setAttribute(name, value);
        }
        return elem[name] || elem.getAttribute(name) || "";
    }
    window["ADS"]["attr"] = attr;

    function create(elem) {
        return document.createElementNS ? document.createElementNS('http://www.w3.org/1999/xhtml', elem) : document.createElement(elem); 0
    }
    window["ADS"]['create'] = create;
    /*
    insertBefore(node,insertnode)
    before(ADS.first(ADS.tag("ol")[0]),li);
    */
    function before(parent, before, elem) {
        if (elem == null) {
            elem = before;
            before = parent;
            parent = before.parentNode;
        }
        var elems = checkElem(elem);
        for (var i = elems.length - 1; i >= 0; i--) {
            parent.insertBefore(elems[i], before);
        }
    }
    window["ADS"]["before"] = before;

    function checkElem(elem) {
        var r = [];
        if (elem.constructor != Array) elem = [elem];
        for (var i = 0; i < elem.length; i++) {
            if (elem[i].constructor == String) {
                var div = document.createElement("div");
                div.innerHTML = elem[i];
                for (var j = 0; j < div.childNodes.length; j++) {
                    r[r.length] = div.childNodes[j];
                }
            }
            else if (elem[i].length) {
                for (var j = 0; j < elem[i].length; j++)
                    r[r.length] = elem[i][j];
            }
            else {
                r[r.length] = elem[i];
            }
        }
        return r;
    }

    function append(parent, elem) {
        var elems = checkElem(elem);
        for (var i = 0; i < elems.length; i++) {
            parent.appendChild(checkElem(elem));
        }
    }
    window["ADS"]["append"] = append;

    function remove(elem) {
        if (elem) elem.parentNode.removeChild(elem);
    }
    window["ADS"]["remove"] = remove;

    function empty(elem) {
        while (elem.firstChild) {
            remove(elem.firstChild);
        }
    }
    window["ADS"]["empty"] = empty;

    function stopBubble(e) {
        if (e && e.stopPropation) {
            e.stopPropation();
        }
        else {
            window.event.cancelBubble = true;
        }
    }
    window["ADS"]["stopBubble"] = stopBubble;

    function stopDefault(e) {
        if (e && e.preventDefault)
            e.preventDefault(e);
        else
            window.event.returnValue = false;
        return false;
    }
    window["ADS"]["stopDefault"] = stopDefault;

    //默认获取所有样式.
    function getStyle(elem, name) {
        if (elem.style[name])
            return elem.style[name];
        else if (elem.currentStyle) {
            return elem.currentStyle[name];
        }
        else if (document.defaultView && document.defaultView.getComputedStyle) {
            name = name.replace(/([A-Z])/g, "-$1");
            name = name.toLowerCase();

            var s = document.defaultView.getComputedStyle(elem, "");
            return s && s.getPropertyValue(name);
        }
        else {
            return null;
        }

    }
    window["ADS"]["getStyle"] = getStyle;

    //获得元素相对于页面位置..,在做全屏动画的时候会有用..
    function pageX(elem) {
        return elem.offsetParent ? elem.offsetLeft + pageX(elem.offsetParent) : elem.offsetLeft;
    }
    window["ADS"]["pageX"] = pageX;

    function pageY(elem) {
        return elem.offsetParent ? elem.offsetTop + pageY(elem.offsetParent) : elem.offsetTop;
    }
    window["ADS"]["pageY"] = pageY;

    //获取元素相对于夫元素的位置

    function parentX(elem) {
        return elem.parentNode == elem.offsetParent ? elem.offsetLeft : pageX(elem) - pageX(elem.parentNode);
    }
    window["ADS"]["parentX"] = parentX;
    function parentY(elem) {
        return elem.parentNode == elem.offsetParent ? elem.offsetTop : pageY(elem) - pageY(elem.parentNode);
    }
    window["ADS"]["parentY"] = parentY;

    //获取CSS定位的像素值
    function posX(elem) {
        return parseInt(getStyle(elem, "left"));
    }
    window["ADS"]["posX"] = posX;
    function posY(elem) {
        return parseInt(getStyle(elem, "top"));
    }
    window["ADS"]["posY"] = posY;
    function setX(elem, pos) {
        elem.style.left = pos + "px";
    }
    window["ADS"]["setX"] = setX;
    function setY(elem, pos) {
        elem.style.top = pos + "px";
    }
    window["ADS"]["setY"] = setY;

    //增加元素的水平位置上增加像素距离的函数...
    function addX(elem, pos) {
        setX(posX(elem) + pos);
    }
    window["ADS"]["addX"] = addX;

    function addY(elem, pos) {
        setY(posY(elem) + pos);
    }
    window["ADS"]["addY"] = addY;

    function getHeight(elem) {
        return getStyle(elem, 'height');
    }
    window["ADS"]["getHeight"] = getHeight;

    function getWidth(elem) {
        return getStyle(elem, 'width');
    }
    window["ADS"]["getWidth"] = getWidth;

    //获取完全的高度,在页面中DOM节点被隐藏的情况之下..
    function fullHeight(elem) {
        if (getStyle(elem, 'display') != "none")
            return elem.offsetHeight || getHeight(elem);
        else {
            var old = resetCSS(elem, {
                display: 'block',
                visibility: 'hidden',
                position: 'absolute'
            });
            var h = elem.clientHeight || getHeight(elem);
            restoreCSS(elem, old);


            return h;
        }
    }
    window["ADS"]["fullHeight"] = fullHeight;

    function fullWidth(elem) {
        if (getStyle(elem, 'display') != "none")
            return elem.offsetWidth || getWidth(elem);
        var old = resetCSS(elem, {
            display: '', //恢复默认样式..display:block...XXX.
            visibility: 'hidden',
            position: 'absolute'
        });
        var w = elem.clientWidth;
        restoreCSS(elem, old);
        return w;
    }
    window["ADS"]["fullWidth"] = fullWidth;

    function resetCSS(elem, prop) {
        var old = {};
        for (var i in prop) {
            old[i] = elem.style[i]; //原有的css样式..
            elem.style[i] = prop[i];
            /*
            赋予新的CSS样式..,elem.style.display='',

                
            */
            /*
            elem.style[display]='';
            elem.style[visibility]=hidden;
            elem.style[position]=absolute;

            */
        }
        return old; //返回旧样式..但在函数中已经把样式
    }

    function restoreCSS(elem, prop) {
        for (var i in prop)
            elem.style[i] = prop[i];
    }
    /*样式设置函数..??
    function setStyle(elem, style, yangshi) {
    var style = [];
    for (var i in style) { 
            
    }
    }
    */

    function hide(elem) {
        var curDisplay = getStyle(elem, "display");
        if (curDisplay != 'none') {
            elem.$oldDisplay = curDisplay; //记录原始状态..
        }
        else
            elem.style.display = 'none';

    }
    window['ADS']['hide'] = hide;

    function show(elem) {
        elem.style.display = elem.$oldDisplay;
    }
    window['ADS']['show'] = show;

    function setOpacity(elem, level) {
        if (elem.filters)
            elem.style.filter = 'alpha(opacity=' + level + ')';
        else
            elem.style.opacity = level / 100;
    }
    window['ADS']['setOpacity'] = setOpacity;


    function slideDown(elem) {
        show(elem);
        var h = fullHeight(elem);
        elem.style.height = "0px";

        for (var i = 0; i <= 100; i += 5) {
            (function () {
                var pos = i;
                setTimeout(function () {
                    elem.style.height = (pos / 100) * h
                }, (pos + 1) * 10);
            })();
        }
    }
    window['ADS']['slideDown'] = slideDown;
    function fadeIn(elem) {
        setOpacity(elem, 0);
        show(elem);
        for (var i = 0; i <= 100; i += 5) {
            (function () {
                var pos = i;
                setTimeout(function () {
                    setOpacity(elem, pos);
                }, (pos + 1) * 10);
            })();

        }

    }
    window['ADS']['fadeIn'] = fadeIn;

    function getX(e) {
        e = e || window.event;
        return e.pageX || e.clientX + document.body.scrollLeft;
    }
    window["ADS"]['getX'] = getX;

    function getY(e) {
        e = e || window.event;
        return e.pageY || e.clientY + document.body.scrollTop;
    }
    window['ADS']['getY'] = getY;

    function getElementX(e) {
        return (e && e.layerX) || window.event.offsetX;
    }
    window['ADS']['getElementX'] = getElementX;
    function getElementY(e) {
        return (e && e.layerY) || window.event.offsetY;
    }
    window['ADS']['getElemetnY'] = getElementY;

    function pageHeight() {
        return document.body.scrollHeight;
    }
    window['ADS']['pageHeight'] = pageHeight;
    function pageWidth() {
        return document.body.scrollWidth;
    }
    window['ADS']['pageWidth'] = pageWidth;
    //获得滚动条的问题..

    function scrollX() {
        var de = document.documentElement;
        return self.pageXOffset || (de && de.scrollLeft) || document.body.scrollLeft;
    }
    window['ADS']['scrollX'] = scrollX;
    function scrollY() {
        var de = document.documentElement;
        return self.pageYOffset || (de && de.scrollTop) || document.body.scrollTop;
    }
    window['ADS']['scrollY'] = scrollY;
    /*
    这里有两个内置方法..
    window.scoll(0,0)
    window.scrollTo(0,**px) 屏幕移动到哪里..
    */
    function windowHeight() {
        var de = document.documentElement;
        return self.innerHeight ||
            (de && de.clientHeight) || document.body.clientHeight;
    }
    window['ADS']['windowHeight'] = windowHeight;
    function windowWidth() {
        var de = document.documentElement;
        return self.innerWidth || (de && de.clientWidth) || document.body.clientWidth;
    }
    window['ADS']['windowWidth'] = windowWidth;

    /*
    拖放函数...使用详见.. http://boring.youngpup.net/projects/dom-drag/
       
    */
    var Drag = {

        obj: null,

        init: function (o, oRoot, minX, maxX, minY, maxY, bSwapHorzRef, bSwapVertRef, fXMapper, fYMapper) {
            o.onmousedown = Drag.start;

            o.hmode = bSwapHorzRef ? false : true;
            o.vmode = bSwapVertRef ? false : true;

            o.root = oRoot && oRoot != null ? oRoot : o;

            if (o.hmode && isNaN(parseInt(o.root.style.left))) o.root.style.left = "0px";
            if (o.vmode && isNaN(parseInt(o.root.style.top))) o.root.style.top = "0px";
            if (!o.hmode && isNaN(parseInt(o.root.style.right))) o.root.style.right = "0px";
            if (!o.vmode && isNaN(parseInt(o.root.style.bottom))) o.root.style.bottom = "0px";

            o.minX = typeof minX != 'undefined' ? minX : null;
            o.minY = typeof minY != 'undefined' ? minY : null;
            o.maxX = typeof maxX != 'undefined' ? maxX : null;
            o.maxY = typeof maxY != 'undefined' ? maxY : null;

            o.xMapper = fXMapper ? fXMapper : null;
            o.yMapper = fYMapper ? fYMapper : null;

            o.root.onDragStart = new Function();
            o.root.onDragEnd = new Function();
            o.root.onDrag = new Function();
        },

        start: function (e) {
            var o = Drag.obj = this;
            e = Drag.fixE(e);
            var y = parseInt(o.vmode ? o.root.style.top : o.root.style.bottom);
            var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right);
            o.root.onDragStart(x, y);

            o.lastMouseX = e.clientX;
            o.lastMouseY = e.clientY;

            if (o.hmode) {
                if (o.minX != null) o.minMouseX = e.clientX - x + o.minX;
                if (o.maxX != null) o.maxMouseX = o.minMouseX + o.maxX - o.minX;
            } else {
                if (o.minX != null) o.maxMouseX = -o.minX + e.clientX + x;
                if (o.maxX != null) o.minMouseX = -o.maxX + e.clientX + x;
            }

            if (o.vmode) {
                if (o.minY != null) o.minMouseY = e.clientY - y + o.minY;
                if (o.maxY != null) o.maxMouseY = o.minMouseY + o.maxY - o.minY;
            } else {
                if (o.minY != null) o.maxMouseY = -o.minY + e.clientY + y;
                if (o.maxY != null) o.minMouseY = -o.maxY + e.clientY + y;
            }

            document.onmousemove = Drag.drag;
            document.onmouseup = Drag.end;

            return false;
        },

        drag: function (e) {
            e = Drag.fixE(e);
            var o = Drag.obj;

            var ey = e.clientY;
            var ex = e.clientX;
            var y = parseInt(o.vmode ? o.root.style.top : o.root.style.bottom);
            var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right);
            var nx, ny;

            if (o.minX != null) ex = o.hmode ? Math.max(ex, o.minMouseX) : Math.min(ex, o.maxMouseX);
            if (o.maxX != null) ex = o.hmode ? Math.min(ex, o.maxMouseX) : Math.max(ex, o.minMouseX);
            if (o.minY != null) ey = o.vmode ? Math.max(ey, o.minMouseY) : Math.min(ey, o.maxMouseY);
            if (o.maxY != null) ey = o.vmode ? Math.min(ey, o.maxMouseY) : Math.max(ey, o.minMouseY);

            nx = x + ((ex - o.lastMouseX) * (o.hmode ? 1 : -1));
            ny = y + ((ey - o.lastMouseY) * (o.vmode ? 1 : -1));

            if (o.xMapper) nx = o.xMapper(y)
            else if (o.yMapper) ny = o.yMapper(x)

            Drag.obj.root.style[o.hmode ? "left" : "right"] = nx + "px";
            Drag.obj.root.style[o.vmode ? "top" : "bottom"] = ny + "px";
            Drag.obj.lastMouseX = ex;
            Drag.obj.lastMouseY = ey;

            Drag.obj.root.onDrag(nx, ny);
            return false;
        },

        end: function () {
            document.onmousemove = null;
            document.onmouseup = null;
            Drag.obj.root.onDragEnd(parseInt(Drag.obj.root.style[Drag.obj.hmode ? "left" : "right"]),
									parseInt(Drag.obj.root.style[Drag.obj.vmode ? "top " : "bottom"]));
            Drag.obj = null;
        },

        fixE: function (e) {
            if (typeof e == 'undefined') e = window.event;
            if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
            if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
            return e;
        }
    };
    this.Drag = function (elem) {
        return Drag.init(elem);
    }
    window['ADS']['Drag'] = this.Drag;


    /* 以下为TOM效果常用函数..
    参数说明：
 
    id: 大容器，默认为focus
    tabId: 按钮容器的id,默认为大容器
    tabTn: 按钮的标签名，默认为li
    tabCn: 按钮的类名
    conId: 切换内容块的容器id,默认为大容器
    conTn: 切换内容块的标签，默认为div
    conCn: 切换内容块的类名
    bns: 额外按钮支持，为数组，例如：['prev','next']
    extra: 需要在当前按钮上附加的结构代码，0为前，1为后，例如[0，'<span />']
    auto: 是否自动播放,默认为不自动0
    current: 显示当前内容，默认为第一个0
    eType: 事件类型，默认为点击click
    interval: 间隔，默认为3秒3000
    curCn: 当前状态类名,默认为 current
    effect: 切换效果，默认为直接切换,可选择滑动(slide)或者渐隐(fade)
    vertical: 切换方向,仅对slide有效，默认为垂直1
    z:设置当前内容块的z轴值，默认为100
 
    调用方法示意：tabs({conId:'fcon',effect:'fade',auto:1,extra:[0,'<span></span>'],bns:['prev','next']})
    — — BY gp5251
    */

    var tabs = function (JSON) {
        return new tabs.init(JSON);
    }
    tabs.init = function (JSON) {
        this.opt = {
            id: 'focus',
            tabTn: 'li',
            conTn: 'div',
            current: 7,// --------将此项改为你想要的组，0-A，1-B，以此类推
            auto: 0,
            vertical: 1,
            eType: 'click',
            interval: 3000,
            curCn: 'current',
            z: 100,
            tempCn: []
        }
        var opt = this.opt; /*默认参数*/
        for (var x in JSON) opt[x] = JSON[x];
        this.current = opt.current;
        this.conId = opt.conId ? opt.conId : opt.id; //不存在li,div,等定位卡，只给定位到focus..
        this.tabId = opt.tabId ? opt.tabId : opt.id;
        this.cont = id(this.conId);  //把focus转到cont.
        this.tabs = opt.tabCn ? getByCn(opt.tabCn, id(this.tabId)) : id(this.tabId).getElementsByTagName(opt.tabTn);
        this.cons = opt.conCn ? getByCn(opt.conCn, id(this.conId)) : id(this.conId).getElementsByTagName(opt.conTn);
        //which effect to use
        this.effectFn = opt.conId && opt.effect && this.effects[opt.effect] ? this.effects[opt.effect] : this.effects['def'];
        if (opt.effect == 'slide') {
            this.Tol = opt.vertical ? 'top' : 'left'; /*控制垂直向左走,还是向右走..*/
            this.ml = this.cons[0][opt.vertical ? 'offsetHeight' : 'offsetWidth'];
        }
        //handle extra htmlstring
        this.nd = opt.extra ? parseHtmlStr(opt.extra[1]) : null;
        // check if the number of tabs equals of the cons's
        if (this.tabs.length != this.cons.length) {
            throw new Error("Match Failed");
            return;
        } else
            this.levels = this.tabs.length;
        //bind events
        var o = this;
        for (var i = 0; i < this.levels; i++)
            (function (n) {
                opt.tempCn[n] = o.tabs[n].className;
                addEvent(o.tabs[n], opt.eType, function () {
                    o.moveTo(n)
                })
            })(i)
        //handle bns
        if (opt.bns && opt.bns.length == 2) {
            addEvent(id(opt.bns[0]), 'click', function () {
                o.prev()
            });
            addEvent(id(opt.bns[1]), 'click', function () {
                o.next()
            })
        }
        this.moveTo(this.current);
    }
    tabs.init.prototype = {
        effects: {
            def: function (o, n) {
                for (var i = 0; i < o.levels; i++) {
                    o.cons[i].style.display = (n == i) ? 'block' : 'none';
                }
                o.opt.auto && (o.runId = setTimeout(function () { o.next() }, o.opt.interval))
            },
            slide: function (o, n, to) {
                var y = parseInt(o.cont.style[o.Tol] ? o.cont.style[o.Tol] : '0px');
                o.end = (y == to) ? 1 : 0;
                if (!o.end) {
                    if (y < to)
                        y += Math.ceil((to - y) / 10);
                    if (y > to)
                        y -= Math.ceil((y - to) / 10);
                    o.cont.style[o.Tol] = y + "px";
                    o.runId = setTimeout(function () {
                        o.effectFn(o, n, to)
                    }, 10)
                } else
                    o.opt.auto && (o.runId = setTimeout(function () { o.next() }, o.opt.interval))
            },
            fade: function (o, n) {
                var cur = 0;
                for (var i = 0; i < o.levels; i++) {
                    o.cons[i].style.display = (n == i) ? 'block' : 'none';
                    o.cons[i].style.zIndex = (n == i) ? o.opt.z : i;
                }
                (function () {
                    if (cur < 100)
                        cur += Math.ceil((100 - cur) / 10);
                    if (cur > 100)
                        cur -= Math.ceil((cur - 100) / 10);
                    setOpacity(o.cons[n], cur);
                    if (cur != 100)
                        o.runId = setTimeout(arguments.callee, 20)
                    else o.opt.auto && (o.runId = setTimeout(function () { o.next() }, o.opt.interval))
                })()
            }
        },
        addNd: [function (e, nd) {
            e.insertBefore(nd, e.firstChild);
        }, function (e, nd) {
            e.appendChild(nd);
        } ],
        moveTo: function (n) {
            var to = 0, n;
            (n > this.levels - 1) && (n = 0);
            (n < 0) && (n = this.levels - 1);
            if (this.opt.effect == 'slide') {
                to = '-' + n * this.ml;
                this.end = 0;
            }
            this.runId && clearTimeout(this.runId);
            for (var i = 0; i < this.levels; i++) {
                if (n == i) {
                    this.tabs[i].className = this.tabs[i].className ? this.opt.curCn + ' ' + this.opt.tempCn[i] : this.opt.curCn;
                    this.nd &&
					this.addNd[this.opt.extra[0]](this.tabs[i], this.nd)
                } else {
                    this.tabs[i].className = this.opt.tempCn[i];
                    if (this.nd)
                        try { this.tabs[i].removeChild(this.nd) } catch (e) { }
                }
            }
            this.current = n;
            this.effectFn(this, n, to);
        },
        prev: function () {
            this.moveTo(--this.current);
        },
        next: function () {
            this.moveTo(++this.current);
        }
    }
    function id(s) {
        return document.getElementById(s);
    }
    function getByCn(cn, from, onlyOne) {
        var re = [], from = from ? ((from.nodeType == 1) ? from.getElementsByTagName('*') : from) : document.getElementsByTagName('*');
        for (var i = 0, l = from.length; i < l; i++) {
            if (hasClass(cn, from[i])) {
                re.push(from[i]);
                if (onlyOne) break;
            }
        }
        return re;
    }
    function hasClass(val, elem) {
        var re = new RegExp("(^|\\s)" + val + "(\\s|$)");
        if (re.test(elem.className))
            return true;
    }
    function addEvent(obj, type, fn) {
        if (obj.addEventListener)
            obj.addEventListener(type, fn, false);
        else if (obj.attachEvent) {
            obj["e" + type + fn] = fn;
            obj.attachEvent("on" + type, function () {
                obj["e" + type + fn]()
            });
        }
    }
    function parseHtmlStr(str) {
        var nd = document.createElement(/<\w+/.exec(str)[0].substr(1)), atts = str.substr(0, str.indexOf('>') + 1).match(/\w+=(['"])[^>]*?\1/g);
        if (atts && atts.length > 0) {
            var i = 0;
            while (atts[i]) {
                var tmp = atts[i].split('=');
                if (tmp[1] = tmp[1].replace(/['"]/g, ''))
                    nd.setAttribute(tmp[0], tmp[1]);
                i++;
            }
        }
        nd.innerHTML = str.substring(str.indexOf('>') + 1, str.lastIndexOf('<')).replace(/^\s+|\s+$/g, '');
        return nd;
    }
    function setOpacity(e, level) {
        if (elem.filters)
            elem.style.filter = 'alpha(opacity=' + level + ')';
        //  filter:alpha(opacity=50);
        else
            elem.style.opacity = level / 100;
    }

    this.tabs = function (JSON) {
        return tabs(JSON);
    }
    window['ADS']['tabs'] = this.tabs;

    /*以下为幻灯图..*/



})();
