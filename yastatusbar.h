// yastatusbar.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-10-16 02:05:34 +0000
// Version: $Id$
// 

#ifndef _YASTATUSBAR_H_
#define _YASTATUSBAR_H_

#include <QStatusBar>

class YaStatusBar : public QStatusBar
{
    Q_OBJECT;
    // Q_PROPERTY(bool sizeGripEnabled READ isSizeGripEnabled WRITE setSizeGripEnabled);

public:
    explicit YaStatusBar(QWidget* parent=0);
    virtual ~YaStatusBar();

    void addWidget(QWidget *widget, int stretch = 0);
    int insertWidget(int index, QWidget *widget, int stretch = 0);
    void addPermanentWidget(QWidget *widget, int stretch = 0);
    int insertPermanentWidget(int index, QWidget *widget, int stretch = 0);
    void removeWidget(QWidget *widget);

//    void setSizeGripEnabled(bool);
//    bool isSizeGripEnabled() const;

    QString currentMessage() const;

public Q_SLOTS:
    void showMessage(const QString &text, int timeout = 0);
    void clearMessage();

Q_SIGNALS:
    void messageChanged(const QString &text);

protected:
    QStatusBar *lbar;
    QStatusBar *rbar;
};

#endif /* _YASTATUSBAR_H_ */
