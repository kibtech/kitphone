
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <string>

////////////
#include "libwebsockets.h"
#include "libwebsockets_extra.h"

/////////
//
//
//

struct libwebsocket_extension libwebsocket_vopp_client_extensions[] = {
	{ /* terminator */
		NULL, NULL, 0
	}
};


int lws_callback_vopp(struct libwebsocket_context * ctx,
                      struct libwebsocket *wsi,
                      enum libwebsocket_callback_reasons reason,
                      void *user, void *in, size_t len)
{
	unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 4096 +
                      LWS_SEND_BUFFER_POST_PADDING];
	int l;

    // WebSocketClient2 *wsc = (WebSocketClient2*)(libwebsocket_context_user_data(ctx));

	switch (reason) {
	case LWS_CALLBACK_CLOSED:
		fprintf(stderr, "mirror: LWS_CALLBACK_CLOSED\n");
        // wsc->lws_connection_closed(wsi);
		break;

	case LWS_CALLBACK_CLIENT_ESTABLISHED:
        // 因为在调用libwebsocket_client_connect的时候，事件循环还没有启动，
        // 所以事件不会传递出来。
        // qLogx()<<"connection state:"<<libwebsocket_get_connect_state(wsi);
        // wsc->lws_new_connection_established(wsi);
		/*
		 * start the ball rolling,
		 * LWS_CALLBACK_CLIENT_WRITEABLE will come next service
		 */

		// libwebsocket_callback_on_writable(ctx, wsi);
		break;

	case LWS_CALLBACK_CLIENT_RECEIVE:
        fprintf(stderr, "rx %d '%s'\n", (int)len, (char *)in);
        // wsc->lws_ws_message_ready(wsi, (char*)in, len);
		break;

	case LWS_CALLBACK_CLIENT_WRITEABLE:
		// l = sprintf((char *)&buf[LWS_SEND_BUFFER_PRE_PADDING],
		// 			"c #%06X %d %d %d;",
		// 			(int)random() & 0xffffff,
		// 			(int)random() % 500,
		// 			(int)random() % 250,
		// 			(int)random() % 24);

		// libwebsocket_write(wsi,
        //                    &buf[LWS_SEND_BUFFER_PRE_PADDING], l, LWS_WRITE_TEXT);

		// /* get notified as soon as we can write again */

		// libwebsocket_callback_on_writable(ctx, wsi);
        libwebsocket_callback_on_writable(ctx, wsi);
		// /*
		//  * Without at least this delay, we choke the browser
		//  * and the connection stalls, despite we now take care about
		//  * flow control
		//  */

		// usleep(200);
		break;

	default:
		break;
	}

    return 0;
}


struct client_session_user_data {
    struct libwebsocket *wsi;
    int seq;
};

/* list of supported protocols and callbacks */
static struct libwebsocket_protocols vopp_client_protocols[] = {
	{
		"vopp",
		lws_callback_vopp,
		sizeof(struct client_session_user_data),
	},
	{  /* end of list */
		NULL,
		NULL,
		0
	}
};


int main(int argc, char **argv)
{
    int iret = 0;
    unsigned short ctx_port = CONTEXT_PORT_NO_LISTEN;
    libwebsocket_context *ctx = NULL;
    libwebsocket *wsi = NULL;
    char tbuf[200] = {0};
    char tbuf2[100] = {0};
    int tcnt = 0;

    int ssl_connection = 2;  // 0 = ws://, 1 = wss:// encrypted, 2 = wss:// allow self signed certs
    char *cert_path = "./seckey/cacert.pem";
    char *key_path = "./seckey/privkey.pem";

    // ctx = libwebsocket_create_context_ex(ctx_port, NULL, vopp_client_protocols,
    //                                                  libwebsocket_vopp_client_extensions, NULL, NULL, -1, -1, 0, 0)
        ctx = libwebsocket_create_context_ex(ctx_port, NULL, vopp_client_protocols,
                                             libwebsocket_vopp_client_extensions, cert_path, key_path,
                                             -1, -1, 0, 0);
    if (ctx == NULL) {
        assert(ctx != NULL);
    }
    fprintf(stderr,"wsc ctx init done.\n");

    // unsigned short port = 18080;
    unsigned short port = 8000;
    const char *host = "202.108.12.212";
    wsi = libwebsocket_client_connect(ctx, host, port, ssl_connection,
                                      "/abcdeeeeeee/", 
                                      host, host,
                                      vopp_client_protocols[0].name, -1);
    if (wsi == NULL) {
        fprintf(stderr, "connect error.\n");
    }

    if (!libwebsocket_client_is_connected(wsi)) {
        fprintf(stderr, "un connected state.\n");
    }

    while (true && wsi != NULL) {
        iret = libwebsocket_service(ctx, 2000);
        fprintf(stderr, "loop next %d \n", iret);

        if (wsi) {
            memset(tbuf, 0, sizeof(tbuf));
            memset(tbuf2, 0, sizeof(tbuf2));
            snprintf(tbuf2, sizeof(tbuf2), "[testmsgaa %d]", ++tcnt);
            strcpy(&tbuf[LWS_SEND_BUFFER_PRE_PADDING], tbuf2);
            // snprintf(&tbuf[LWS_SEND_BUFFER_PRE_PADDING], sizeof(tbuf)-LWS_SEND_BUFFER_PRE_PADDING, "testmsgaa %d", ++tcnt);
            iret = libwebsocket_write(wsi, (unsigned char*)&tbuf[LWS_SEND_BUFFER_PRE_PADDING],
                                      strlen(tbuf+LWS_SEND_BUFFER_PRE_PADDING), LWS_WRITE_TEXT);
            fprintf(stderr, "send msg: %s, %d\n", (char*)(&tbuf[LWS_SEND_BUFFER_PRE_PADDING]), iret);
            
            sleep(1);
            // break;
        }
    }
    return 0;
}

