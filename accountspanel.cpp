#include "simplelog.h"
#include "global.h"
#include "preferences.h"

#include "ui_accountspanel.h"
#include "accountspanel.h"

AccountsPanel::AccountsPanel(QWidget *parent)
    : QWidget(parent)
    , uiw(new Ui::AccountsPanel())
{
    this->uiw->setupUi(this);

    QObject::connect(this->uiw->listWidget, SIGNAL(itemClicked(QListWidgetItem*)),
                     this, SLOT(onItemClicked(QListWidgetItem*)));

    QListWidgetItem *item = new QListWidgetItem;
    item->setIcon(QIcon(":/skins/images.jpeg"));
    item->setText("hahahaha");

    this->uiw->listWidget->addItem(item);

    QSize sz = this->baseSize();
    qDebug()<<sz;
    sz.setWidth(sz.width() + 200);
    this->setBaseSize(sz);
    // this->setFixedWidth(500);
}

AccountsPanel::~AccountsPanel()
{
    delete this->uiw;
}

// set proper size of this, then popup will have proper size.

// not called???
// void AccountsPanel::show()
// {

//     QSize sz = this->size();
//     this->resize(sz.width() + 500, sz.height());

//     qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<""<<sz<<this->size();

//     QWidget::show();    
// }

QSize AccountsPanel::sizeHint() const
{
    QSize sz;
    
    int cnt = this->uiw->listWidget->count();
    int nwidth = (64 + 6)*5;
    int nheight = (cnt/5 + 1) * (64 + 6 + 30);
    if (cnt/5 + 1 > 5) {
        // 超过5计，不再按照这种方式计算。
        nheight = 5 * (64 + 6 + 30);
    }

    sz = this->size();
    qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<""<<sz<<this->size();
    // this->setFixedWidth(500);
    // sz.setWidth(500);
    sz.setWidth(nwidth);
    sz.setHeight(nheight);
    return sz;
}

bool AccountsPanel::addAccount(QString name, int type)
{
    if (this->mhaccs.contains(name, type)) {
        //// nothing to do
    } else {
        this->mhaccs.insertMulti(name, type);
        QListWidgetItem *item = new QListWidgetItem(0, QListWidgetItem::UserType + type);
        if (type == Preferences::NVE_PJSIP) {
            item->setIcon(QIcon(":/skins/images2.jpeg"));
        } else if (type == Preferences::NVE_SKYCIT) {
            item->setIcon(QIcon(":/skype-kitphone.ico"));
        } else {
            item->setIcon(QIcon(":/skins/default/ask_32.png"));
        }
        item->setText(name);
        item->setToolTip(tr("%1's User %2").arg(type).arg(name));
        this->uiw->listWidget->addItem(item);
    }
    return true;
}

bool AccountsPanel::removeAccount(QString name, int type)
{
    if (this->mhaccs.contains(name, type)) {
        this->mhaccs.remove(name, type);
        QListWidgetItem * item = NULL;
        for (int i = this->uiw->listWidget->count()-1; i >=0 ; --i) {
            item = this->uiw->listWidget->item(i);
            if (item->text() == name && item->type() == QListWidgetItem::UserType + type) {
                item = this->uiw->listWidget->takeItem(i);
            }
        }
    } else {
        qLogx()<<"account not found:"<<name<<type;
    }
    return true;
}

void AccountsPanel::onItemClicked(QListWidgetItem *item)
{
    Q_ASSERT(item != NULL);
    if (item->type() < QListWidgetItem::UserType) {
        // maybe test item without set type
        qLogx()<<"test account item seelcted."<<item->text()<<item->type();
    } else {
        QString name = item->text();
        int type = item->type()-QListWidgetItem::UserType;

        Q_ASSERT(this->mhaccs.contains(name, type));
        emit this->accountSelected(name, type);
    }
}
