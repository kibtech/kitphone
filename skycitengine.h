// skycitengine.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-09-15 09:32:07 +0000
// Version: $Id: skycitengine.h 1044 2011-10-13 06:57:20Z drswinghead $
// 

#ifndef _SKYCITENGINE_H_
#define _SKYCITENGINE_H_

#include <assert.h>
#include <boost/thread/thread.hpp>
// #include "boost/smart_ptr/enable_shared_from_this2.hpp"
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>
#include <boost/bimap.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/condition_variable.hpp>

#include <QtCore>

#include "intermessage.h"

class Skycit;
class WebSocketServer3;
class WebSocketClient3;

class SCBSignalBridge;

class SkycitEngine : public QThread
{
    Q_OBJECT;
protected:
    explicit SkycitEngine(QObject * parent = 0);
    static SkycitEngine *m_inst;
public:
    static SkycitEngine *instance();
    virtual ~SkycitEngine();

public slots:
    // 初始化该类实例持久成员
    // 但调用这个不会引起该实例处理实际事务，也不会产生任何对外事件请求。
    bool initialize();
    bool destroy();

    bool vestart();
    bool vestop();

    // bool isRunning();
    bool isStarted();

    // void runin();
    void onSCEThreadStarted();

    // qt slot, backend server callbacks
    void onWS_websocket_error(int eno);
    void onWS_websocket_started();
    // void onWS_new_websocket_connection();
    void onWS_new_websocket_connection_with(int cseq);
    void onWS_websocket_message(const QString &msg, int cseq);
    void onWS_websocket_connection_closed(int cseq);

    bool process_backend_ws_ctrl_message(const std::string &msg, int cseq);

    /////////////// ws client callbacks
    void onon_ws_client_connected(QString rpath);
    void onon_ws_client_disconnected();
    void onon_ws_client_error();
    void onon_ws_client_message(QByteArray msg);

public:
    virtual void run();

protected slots:
    void onConnectSkype();
    void onConnectApp2App();

    void onSkypeNotFound();
    void onSkypeConnected(QString user_name);
    void onSkypeRealConnected(QString user_name);
    void onSkypeUserStatus(QString str_status, int int_status);
    void onSkypeError(int code, QString msg, QString cmd);

    void onSkypeApp2AppCreated(const QString &appName);
    void onNewStreamCreated(const QString &appName, const QString &contact, int sid);
    void onStreamClosed(const QString &appName);
    void onStreamConnectingDone(const QString &appName);
    void onStreamData(const QString &appName, const QString &contact);

    bool onApp2AppMessage(const QString &appName, const QString &contact, const QString &msg);

    void onSkypeCallArrived(QString callerName, QString calleeName, int callID);
    void onSkypeCallHangup(QString callerName, QString calleeName, int callID);

signals:
    // void runin_it();
    void skycit_engine_started();
    void skycit_engine_error(int eno);

public:
    Skycit *sct;
    QString app_name_prefix;
    QString app_name_main;
    QString app_name_self;

    QHash<QString, QHash<QString,int> > app2appConns; //

private:
    class A2AStreamUnit {
    public:
        A2AStreamUnit() {
            sid = -1;
            connecting_done_evt = false;
            use_finished = false;
        }

        int sid;
        bool connecting_done_evt;
        bool use_finished;

    };
    // 在创建一个新连接之前插入，在connecting_done事件与use_finished事件修改
    // 用于防止自动重连。
    QHash<QString, QHash<QString, A2AStreamUnit> > app2appStates;


    bool _addStream(const QString &appName, const QString &contact, int sid);
    bool _removeStream(const QString &appName, const QString &contact, int sid);
    int  _getStream(const QString &appName, const QString &contact);

protected:
    // boost::signals2::signal<void(int)> sip_engine_error;
    // boost::signals2::signal<void()> sip_engine_started;
    boost::signals2::connection ws_sigc_error;
    boost::signals2::connection ws_sigc_started;
    boost::signals2::connection ws_sigc_new_conn;
    boost::signals2::connection ws_sigc_msg;
    boost::signals2::connection ws_sigc_close;
    
    SCBSignalBridge *sigbr;

    QThread *old_thread;

    QString skygw_name; // 类似固定服务器ip的功能。
    WebSocketServer3 *mws;
    WebSocketClient3 *wsc2;

    class SessionUnit {
    public:
        SessionUnit();
        ~SessionUnit();

        // libwebsocket *wsi;
        int cseq;

        int acc_id;
        int call_id;
        
        std::string mc_str;
        CmdMakeCall cmd_mc;

        std::string reg_str;
        CmdRegister cmd_reg;
    };

    // 只接受一个ws客户端。
    SessionUnit su1;
    
    std::vector<SessionUnit*> scs;

protected:
};

class SCBSignalBridge : public QThread
{
    Q_OBJECT;
public:
    explicit SCBSignalBridge(QObject *parent = 0);
    virtual ~SCBSignalBridge();

    // boost slot
    void on_websocket_error(int eno);
    void on_websocket_started();
    // void on_new_websocket_connection();
    void on_new_websocket_connection_with(int cseq);
    void on_websocket_message(const std::string &msg, int cseq);
    void on_websocket_connection_closed(int cseq);

signals: // qt
    void websocket_error(int eno);
    void websocket_started();
    // void new_websocket_connection();
    void new_websocket_connection_with(int cseq);
    void websocket_message(const QString &msg, int cseq);
    void websocket_connection_closed(int cseq);
};


///////////////////

#endif /* _SKYCITENGINE_H_ */
