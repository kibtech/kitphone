# akad.pro --- 
# 
# Author: liuguangzhao
# Copyright (C) 2007-2012 liuguangzhao@users.sf.net
# URL: 
# Created: 2011-10-20 03:51:22 +0000
# Version: $Id$
# 

QT -= core gui
TARGET = zakad
TEMPLATE = app #lib
CONFIG += debug_and_release console link_pkgconfig #staticlib

QMAKE_CFLAGS += -I/usr/lib/wx/include/gtk2-unicode-release-2.8 -I/usr/include/wx-2.8 -D_FILE_OFFSET_BITS=64 -D_LARGE_FILES -D__WXGTK__ -pthread
QMAKE_CXXFLAGS = ${QMAKE_CFLAGS}

DEFINES += _FILE_OFFSET_BITS=64 _LARGE_FILES __WXGTK__ USE_STD_STRING
INCLUDEPATH += /usr/lib/wx/include/gtk2-unicode-release-2.8 /usr/include/wx-2.8 \
    ./src/libs ./src/include
LIBS += -pthread -Wl,--hash-style=gnu -Wl,--as-needed  -lwx_gtk2u_richtext-2.8 -lwx_gtk2u_aui-2.8 -lwx_gtk2u_xrc-2.8 -lwx_gtk2u_qa-2.8 -lwx_gtk2u_html-2.8 -lwx_gtk2u_adv-2.8 -lwx_gtk2u_core-2.8 -lwx_baseu_xml-2.8 -lwx_baseu_net-2.8 -lwx_baseu-2.8

SOURCES = main.cpp \
    src/kademlia/net/KademliaUDPListener.cpp src/kademlia/net/PacketTracking.cpp \
    src/kademlia/utils/UInt128.cpp \
    src/kademlia/routing/Contact.cpp  src/kademlia/routing/RoutingBin.cpp  src/kademlia/routing/RoutingZone.cpp \
    src/kademlia/kademlia/Entry.cpp     src/kademlia/kademlia/Prefs.cpp      \
    src/kademlia/kademlia/UDPFirewallTester.cpp \
    src/kademlia/kademlia/Indexed.cpp   src/kademlia/kademlia/Search.cpp  \
    src/kademlia/kademlia/Kademlia.cpp  src/kademlia/kademlia/SearchManager.cpp

# SOURCES += src/Tag.cpp # ./src/libs/common/Format.cpp
